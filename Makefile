override CC     := g++
override CCOPTS := -ansi -pedantic -Wall -Wextra -Iinc/ -I./ -DENABLE_OPENMP -fopenmp -O3 $(CCOPTS) 
override LDOPTS := $(LDOPTS) -lX11
override DEPDIR := .deps
override CPP    := $(CC) -E $(CCOPTS) # pre-processor

################################################################################
# Dependency generation stuff
################################################################################

# Make dependency directory
$(DEPDIR):
	@mkdir -p $(DEPDIR)

# Function to create dependency information using C-preprocessor
# Looks for lines of the form # 1 "inc/verify.h" 1 from pre-preprocessor
DEPFILE    = $(DEPDIR)/$(@F).d
# Need $(CCOPTS) for include paths
MAKEDEPEND = $(CPP) $(CCOPTS) $< \
	| sed -n 's/^\# *[0-9][0-9]* *"\([^<"]*\)".*/bin\/$(@F): \1/p' \
	| sort | uniq > $(DEPFILE)

# This goes line by line in the .d file and generates an empty make target
# for each depenency.  Then, if the dependency moves or changes names, make
# will just regenerate them instead of throwing a no such target error.
MAKEDUMMY  = cp $(DEPDIR)/$(@F).d $(DEPDIR)/$(@F).P; \
	sed -e 's/\#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
	    -e '/^$$/ d'  -e 's/$$/ :/' < $(DEPDIR)/$(@F).d >> $(DEPDIR)/$(@F).P; \
	rm -f $(DEPDIR)/$(@F).d	


################################################################################
# Ignore all that and add your programs here
################################################################################
PROGRAMS :=             \
	bin/lion

all : $(DEPDIR) $(PROGRAMS)

# General binary target
bin/% : src/%.cc
	@$(MAKEDEPEND)	         
	@$(MAKEDUMMY)
	$(CC) $(CCOPTS) $< -o $@ $(LDOPTS) 

depclean:
	@rm -f $(DEPDIR)/*

clean: 
	@rm -f $(PROGRAMS)

remake: clean all

# Include generated dependencies
-include $(patsubst bin/%,$(DEPDIR)/%.P,$(PROGRAMS))
