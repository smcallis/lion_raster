A simple experiment in software rasterization.  Using the simplex library for underlying 
window/canvas support.  This demo rasterizes the lion from Antigrain Geometry while it's 
manipulated by the users mouse. 

Click and drag  near a corner of the lion to move that corner and watch as the rasterizer
handles the transformed geometry flawllessly.

on most reasonable systems (not windows):
> make 
> bin/lion

Requires libX11