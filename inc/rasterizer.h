#ifndef RASTERIZER_H_
#define RASTERIZER_H_

// contains implementation of cl-aa algorithm (same algorithm used in Antigrain Geometry) for antialised polygon rasterization
//
// the rasterize_polygon function is almost surely what you're looking for

#include <simplex.h>

#ifdef DEBUGON
#define DEBUG(x) x
#else
#define DEBUG(x)
#endif

// sub-pixel resolution in x/y direction
const int32_t SWRAST_SHIFT    = 8;
const int32_t SWRAST_MASK     = 0xFF; // (1 << SWRAST_SHIFT) - 1
const int32_t SWRAST_SUBPIXEL = 256;  //  1 << SWRAST_SHIFT

// XXX: area can be > 16-bit int..
typedef struct {
    int16_t x,y;   // cell coordinates
    int32_t cover; // number of sub-pixel lines spanned by line segment in cell
    int32_t area;  // area in sub-pixels from left-side of cell to line segment
} swrast_cell_t;


// build new swrast_cell_t with given coordinates
static inline swrast_cell_t swrast_cell(int16_t x, int16_t y) {
    swrast_cell_t cell = {x,y,0,0};
    return cell;
}


// compare two rast_cell_t instances to sort by y, then x
static inline int swrast_cell_cmp(const void *pa, const void *pb) {
    const swrast_cell_t *aa = (const swrast_cell_t*)pa;
    const swrast_cell_t *bb = (const swrast_cell_t*)pb;

    if (aa->y < bb->y) return -1;
    if (aa->y > bb->y) return +1;
    if (aa->x < bb->x) return -1;
    if (aa->x > bb->x) return +1;
    return 0;
}


// struct that holds state for the rasterizer between calls
typedef struct {
    swrast_cell_t *cells;   // array of cells that grows as needed
    size_t         ncell;   // number of cells in array
    size_t         cellcap; // total capacity of the cell array
} swrast_t;


// initialize a new software rasterizer instance
static inline swrast_t swrast_init() {
    swrast_t rast;
    rast.ncell   = 0;
    rast.cellcap = 32;
    rast.cells   = (swrast_cell_t*)calloc(rast.cellcap, sizeof(swrast_cell_t));
    return rast;
}


// cleanup a software rasterizer instance
static inline void swrast_free(swrast_t *rast) {
    free(rast->cells);
    rast->ncell   = 0;
    rast->cellcap = 0;
}


/*******************************************************************************
 * Update the value of the last cell in the list.  If the last cells x,y matches
 * what's passed to the function, we accumulate cover and area values.  If the
 * coordinates don't match, we create a new cell and add it to the list.
 *
 * @param swrast  pointer to swrast_t instance we're operating in
 * @param xx      x coordinate of cell
 * @param yy      y coordinate of cell
 * @param cover   cover value
 * @param area    area value
 ********************************************************************************/
static inline void swrast_add_cell(swrast_t *swrast, int16_t xx, int16_t yy, int32_t cover, int32_t area) {
    if (cover == 0) return;

    if (swrast->ncell == swrast->cellcap) {
        swrast->cellcap  = 2*swrast->cellcap;
        swrast->cells    = (swrast_cell_t*)realloc(swrast->cells, swrast->cellcap*sizeof(swrast_cell_t));
    }
    
    swrast_cell_t cell = { xx, yy, cover, area};
    swrast->cells[swrast->ncell++] = cell;
}


/*******************************************************************************
 * Compute q and r, s.t  x = q*y+r the quotient q is rounded to negative infinity
 * this ensures the remainder is always positive.
 *******************************************************************************/
static inline void int_div_floor(int32_t x, int32_t y, int32_t *q, int32_t *r) {
    *q = x/y;
    *r = x-*q*y;

    if (*r < 0) {
        *q -= 1;
        *r += y;
    }
}


/*******************************************************************************
 * Take a line segment in sub-pixel coordinates and break it into segments at each
 * pixel crossing (ie where it enters a new cell).  Each segment is passed 
 * to a function of the form (swrast, xx, yy, a0, b0f, a1, b1f) where swrast is a pointer
 * to the current software rasterizer context, xx and yy are the current cell x/y coordinates
 * and a0,a1 are whole valued coordinates and b0f,b1f are fractional coordinates.  
 *
 * Note that coordinates are specified as a,b rather than x,y as this function is used twice:
 * once to split on the y pixel boundaries and again to split on the x pixel boundaries.
 *
 * @param swrast    current software rasterizer context
 * @param yy        cell y coordinate for line segment (should be called with zero at top level)
 * @param x0        starting x coordinate of line in sub-pixel coordinates
 * @param y0        starting y coordinate of line in sub-pixel coordinates
 * @param x1        ending x coordinate of line in sub-pixel coordinates
 * @param y1        ending y coordinate of line in sub-pixel coordinates
 * @param function  call back to call with each line segment
 *******************************************************************************/
static inline void swrast_map_line_segments(
    swrast_t *swrast, int32_t yy, int32_t a0, int32_t b0, int32_t a1, int32_t b1,
    void (*function)(swrast_t*, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t)) {
    
    // split b coordinates into whole and fractional parts
    int32_t b0w, b0f;  int_div_floor(b0, SWRAST_SUBPIXEL, &b0w, &b0f);
    int32_t b1w, b1f;  int_div_floor(b1, SWRAST_SUBPIXEL, &b1w, &b1f);
    
    if (b0w == b1w) {
        // line segment doesn't cross pixel boundary, just call function and we're done
        // this handles the case where line is completely flat as well (ie b0 == b1)

        // XXX: can we safely not call when b0f == b1f?
        function(swrast, b0w, yy, a0, b0f, a1, b1f);    
    } else { 
        // line segment does cross pixel boundaries, so we have to split it up into segments
        // this is done by incrementing in the b coordinate and computing the a coordinate as a function
        // of the line slope
        int32_t delta_a     = a1-a0;
        int32_t delta_b     = (b0 < b1) ? b1-b0 : b0-b1; // abs(b1-b0)
        int32_t binc        = (b0 < b1) ? +1 : -1;
        int32_t lo_boundary = (b0 < b1) ? 0               : SWRAST_SUBPIXEL;
        int32_t hi_boundary = (b0 < b1) ? SWRAST_SUBPIXEL : 0;

        // compute first fractional part of line segment
        int32_t db = (b0 < b1) ? SWRAST_SUBPIXEL - b0f : b0f;
        int32_t aa = delta_a*db + delta_b/2; 
        int32_t aw, af;  int_div_floor(aa, delta_b, &aw, &af);
        aw += a0;

        if (b0f != hi_boundary) function(swrast, b0w, yy, a0, b0f, aw, hi_boundary);
        
        // increment in b direction and compute segments
        int32_t bb    = b0w + binc;
        int32_t ainc  = delta_a*SWRAST_SUBPIXEL;
        int32_t step, mod;  int_div_floor(ainc, delta_b, &step, &mod);
        
        if (bb != b1w) { 
            do {
                int32_t preva = aw;
                aw += step;
                af += mod;
                
                if (af >= delta_b) {
                    aw++;
                    af -= delta_b;
                }

                function(swrast, bb, yy, preva, lo_boundary, aw, hi_boundary);
                bb += binc;
            } while (bb != b1w);
        }

        function(swrast, bb, yy, aw, lo_boundary, a1, b1f);
    }
}


// callback from swrast_map_line_horizontal for swrast_map_line_segments, receives
// the fully divided line segment (split on x and y pixel boundaries) and updates the
// appropriate cells area and cover.  Note that we receive x/y coordinates reversed
// due to how we call swrast_map_line_segments twice to split the line.
static inline void swrast_handle_segment(
    swrast_t *swrast, int32_t xx, int32_t yy, int32_t y0f, int32_t x0f, int32_t y1f, int32_t x1f
) {
    int32_t cover = y1f-y0f;
    int32_t area = (x0f+x1f)*cover/2;
    swrast_add_cell(swrast, xx, yy, cover, area);
}


// callback from rasterize_polygon for swrast_map_line_segments.  Swaps coordinates and calls
// swrast_map_line_segments again to split line segment by x boundaries
static inline void swrast_map_line_horizontal(
    swrast_t *swrast, int32_t yy, int32_t /*na*/, int32_t x0, int32_t y0f, int32_t x1, int32_t y1f
) {
    swrast_map_line_segments(swrast, yy, y0f, x0, y1f, x1, swrast_handle_segment);
}


// clipped absolute value of alpha 
int64_t non_zero_alpha(int64_t eff_alpha) {
    if (eff_alpha < 0) {
        eff_alpha = -eff_alpha;
    }
    eff_alpha = (eff_alpha > 256) ? 256 : eff_alpha;
    return eff_alpha;
} 


// handle the even/odd alpha filling
int64_t even_odd_alpha(int64_t eff_alpha) {
    int32_t ew,ef; int_div_floor(eff_alpha, 512, &ew, &ef);
    ef = (ef <= 256) ? ef : 512 - ef;
    return (ef > 256) ? 256 : ef;
} 


// threshold alpha to 0/1
int64_t bool_alpha(int64_t value) {
    if (value < 0) {
        value = -value;
    }
    if (value > 128) {
        return 256;
    } else {
        return 0;
    }
}

static bool antialiased = true;
static bool even_odd    = true;

// attenuate alpha by coverage amount
static inline uint8_t swrast_alpha(int64_t cover, int64_t area, uint8_t alpha) {
    int64_t eff_alpha  = (SWRAST_SUBPIXEL*cover - area) / (SWRAST_SUBPIXEL*SWRAST_SUBPIXEL/256); // 256 is alpha range */
    eff_alpha = even_odd ? even_odd_alpha(eff_alpha) : non_zero_alpha(eff_alpha);
    if (!antialiased) { 
        eff_alpha = bool_alpha(eff_alpha);
    }
    
    return (uint8_t)(eff_alpha*alpha >> 8);
}


/*******************************************************************************
 * rasterize polygon(s) using the cl-aa algorithm to a pixbuffer.  Does not
 * call the pixbuf_redraw function on the pixbuffer.  
 *
 * @param swrast  software rasterizer instance to work in
 * @param pb      pixbuffer to render to
 * @param color   color to rasterize polygon(s) with
 * @param points  array of points specifying the polygon(s) (x/y interleaved)
 * @param npoint  total number of points in array
 * @param offsets offset array specifying start of polygon NULL implies single polygon
 * @param npoly   total number of polygons (1 if offets == NULL)
 *******************************************************************************/
static inline void rasterize_polygon(
    swrast_t *swrast, pixbuf_t *pb, pixel_t color, 
    double   *points, size_t npoint, size_t *offsets, size_t npoly) {

    // if NULL is passsed for offset array, just draw one polygon starting at zero.
    // this is just a convenience so the user doesn't have to declare a dummy variable to pass in for a single polygon.
    size_t  offset_zero = 0;
    size_t *poffset     = offsets;
    if (offsets == NULL) {
        poffset = &offset_zero;
        npoly   = 1;
    }

    // reset rasterization context
    swrast->ncell = 0;
    
    // rasterize polygons into cells with coverage information
    for (size_t poly=0; poly < npoly; poly++) {
        size_t beg = poffset[poly];
        size_t end = (poly == npoly-1) ? npoint : poffset[poly+1];

        // iterate over line segments in polygon, stride by two for x/y per point
        for (size_t ii=beg; ii < end-1; ii++) {
            // convert line coordinates to integer in subpixel coordinates
            int32_t x0 = (int32_t)(points[2*(ii+0)+0]*SWRAST_SUBPIXEL); // gcc seems to have a hard time inlining lrintf() calls
            int32_t y0 = (int32_t)(points[2*(ii+0)+1]*SWRAST_SUBPIXEL); // on 64-bit these casts get turned into cvtss2si instructions
            int32_t x1 = (int32_t)(points[2*(ii+1)+0]*SWRAST_SUBPIXEL);
            int32_t y1 = (int32_t)(points[2*(ii+1)+1]*SWRAST_SUBPIXEL);
            
            swrast_map_line_segments(swrast, 0, x0, y0, x1, y1, swrast_map_line_horizontal);
         }
    }
   
    // sort cells
    qsort(swrast->cells, swrast->ncell, sizeof(swrast_cell_t), swrast_cell_cmp);

    
    uint8_t base_alpha = color.a;
    pixel_t base_color = pixel_unset_alpha(color);

    ssize_t xx    = swrast->cells[0].x;
    ssize_t yy    = swrast->cells[0].y;
    ssize_t cover = 0;
    ssize_t area  = 0;
    swrast_cell_t *cells = swrast->cells;

    size_t ii=0;
    while (ii < swrast->ncell) {
        while (ii < swrast->ncell && cells[ii].y < pb->cliprect.y0) {
            ii++;
            area  = 0;
            cover = 0;
            xx    = cells[ii].x;
            yy    = cells[ii].y;            
        }

        // accumulate cells with same coordinates
        while (ii < swrast->ncell && cells[ii].x == xx && cells[ii].y == yy) {
            cover += cells[ii].cover;
            area  += cells[ii].area;
            ii++;
        }

        // now we're on the next cell.  draw first pixel and pixels that get us here
        uint8_t alpha = swrast_alpha(cover, area, base_alpha);
        draw_pixel_clip(pb, pixel_set_alpha(base_color, alpha), xx, yy);

        if (ii == swrast->ncell) {
            break;
        }
        
        if (cells[ii].x > xx+1) {
            uint8_t alpha = swrast_alpha(cover, 0, base_alpha);
            pixel_t color = pixel_set_alpha(base_color, alpha);

            ssize_t xlo = pixbuf_clip_x(pb, xx+1);
            ssize_t xhi = pixbuf_clip_x(pb, cells[ii].x);
            
            pixel_t *pix = &pb->pixels[yy*pb->stride+xlo];
            //for (ssize_t xc=xx+1; xc < cells[ii].x; xc++) {
            for (ssize_t xc=xlo; xc < xhi; xc++) {
                //draw_pixel_clip(pb, color, xc, yy);
                *pix = blend_pixel(*pix, color);
                pix++;
            }                        
        }

        // and start on new cell
        area = 0;
        if (cells[ii].y != yy) { // cover should naturally sum to zero, check if it doesn't?
            // XXX: debug
            if (cover != 0) {
                printf("scanline %i -- non-zero final cover\n", (int)yy);
                exit(-1);
            }
            cover = 0;
        }
        xx = cells[ii].x;
        yy = cells[ii].y;

        if (yy >= pb->cliprect.y1) break;
    }
}





#endif//RASTERIZER_H_
