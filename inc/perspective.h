#ifndef PERSPECTIVE_H_
#define PERSPECTIVE_H_

#include <perspective.h>

// contains code for quadrilaterals and computing perspective transformations


// struct repepresenting a quadrilateral
typedef struct {
    point_t p0,p1,p2,p3;
} quadrilateral_t;


// struct representing a perspective transformation
typedef struct {
    double sx,  shy, w0;
    double shx, sy,  w1;
    double tx,  ty,  w2;
} perspective_t;


// build a quad out of four points
static inline quadrilateral_t quadrilateral(point_t p0, point_t p1, point_t p2, point_t p3) {
    quadrilateral_t quad = {p0,p1,p2,p3};
    return quad;
}


// build a quad out of a rectangle
static inline quadrilateral_t quadrilateral(rect_t rect) {
    quadrilateral_t quad = {point(rect.x0, rect.y0),
                            point(rect.x1, rect.y0),
                            point(rect.x1, rect.y1),
                            point(rect.x0, rect.y1)};
    return quad;
}


// build new perspective from points
static inline perspective_t perspective(
    double sx,  double shy, double w0,
    double shx, double sy,  double w1,
    double tx,  double ty,  double w2
) {
    perspective_t ans = {
        sx,  shy, w0,
        shx, sy,  w1,
        tx,  ty,  w2
    };
    return ans;
}


// compute new perspective transformation as A*B
static inline perspective_t operator *(perspective_t aa, perspective_t bb) {
    perspective_t ans;
    ans.sx  = aa.sx *bb.sx  + aa.shy*bb.shx + aa.w0*bb.tx;
    ans.shy = aa.sx *bb.shy + aa.shy*bb.sy  + aa.w0*bb.ty;
    ans.w0  = aa.sx *bb.w0  + aa.shy*bb.w1  + aa.w0*bb.w2;
    ans.shx = aa.shx*bb.sx  + aa.sy *bb.shx + aa.w1*bb.tx;
    ans.sy  = aa.shx*bb.shy + aa.sy *bb.sy  + aa.w1*bb.ty;
    ans.w1  = aa.shx*bb.w0  + aa.sy *bb.w1  + aa.w1*bb.w2;
    ans.tx  = aa.tx *bb.sx  + aa.ty *bb.shx + aa.w2*bb.tx;
    ans.ty  = aa.tx *bb.shy + aa.ty *bb.sy  + aa.w2*bb.ty;
    ans.w2  = aa.tx *bb.w0  + aa.ty *bb.w1  + aa.w2*bb.w2;
    return ans;    
}


// transform a point p by the perspective transform xfrm
static inline point_t transform(perspective_t xfrm, point_t p) {
    double mm = 1.0/(p.x*xfrm.w0 + p.y*xfrm.w1 + xfrm.w2);
    return point(
        mm*(p.x*xfrm.sx  + p.y*xfrm.shx + xfrm.tx),
        mm*(p.x*xfrm.shy + p.y*xfrm.sy  + xfrm.ty)
    );
}


// transform an array of points
static inline void transform(perspective_t xfrm, point_t *points, size_t npoint) {
    for (size_t ii=0; ii < npoint; ii++) {
        points[ii] = transform(xfrm, points[ii]);
    }
}


// invert a perspective transformation.
static inline perspective_t invert(perspective_t aa) {
    double d0  = aa.sy *aa.w2 - aa.w1 *aa.ty;
    double d1  = aa.w0 *aa.ty - aa.shy*aa.w2;
    double d2  = aa.shy*aa.w1 - aa.w0 *aa.sy;
    double det = aa.sx*d0 + aa.shx*d1 + aa.tx*d2;

    if(det == 0.0) {
        return perspective(
            0.0, 0.0, 0.0, 
            0.0, 0.0, 0.0, 
            0.0, 0.0, 0.0
        );
    }    
    
    det = 1.0/det;
    return perspective(
        det*d0,                            det*d1,                            det*d2,
        det*(aa.w1 *aa.tx - aa.shx*aa.w2), det*(aa.sx *aa.w2 - aa.w0 *aa.tx), det*(aa.w0 *aa.shx - aa.sx *aa.w1),
        det*(aa.shx*aa.ty - aa.sy *aa.tx), det*(aa.shy*aa.tx - aa.sx *aa.ty), det*(aa.sx *aa.sy  - aa.shy*aa.shx)
    );
}


// compute transform from the unit square to a quad
static inline perspective_t square_to_quad(quadrilateral_t q) {
    double dx = q.p0.x - q.p1.x + q.p2.x - q.p3.x;
    double dy = q.p0.y - q.p1.y + q.p2.y - q.p3.y;

    if(dx == 0.0 && dy == 0.0) {
        // quad is a parallelogram, return an affine transformation
        return perspective(
            q.p1.x-q.p0.x,  q.p1.y-q.p0.y,  0.0,
            q.p2.x-q.p1.x,  q.p2.y-q.p1.y,  0.0,
            q.p0.x,         q.p0.y,         1.0
        );
    } else {
        double dx1 =  q.p1.x - q.p2.x;
        double dy1 =  q.p1.y - q.p2.y;
        double dx2 =  q.p3.x - q.p2.x;
        double dy2 =  q.p3.y - q.p2.y;
        double den = dx1*dy2 - dx2*dy1;

        // check for ill conditioning
        if (den == 0.0) {
            return perspective(
                0.0, 0.0, 0.0,
                0.0, 0.0, 0.0,
                0.0, 0.0, 0.0
            );
        } 
                
        // build the perspective transform.
        double u = (dx*dy2 - dy*dx2)/den;
        double v = (dy*dx1 - dx*dy1)/den;
        return perspective(
            q.p1.x - q.p0.x + u*q.p1.x, q.p1.y - q.p0.y + u*q.p1.y, u, 
            q.p3.x - q.p0.x + v*q.p3.x, q.p3.y - q.p0.y + v*q.p3.y, v,
            q.p0.x,                     q.p0.y,                     1.0
        );
    }
}


// compute transform from a quad to the unit square
static inline perspective_t quad_to_square(quadrilateral_t q) {
    return invert(
        square_to_quad(q)
    );
}


// compute transform mapping between two arbitrary quadrilaterals
static inline perspective_t quad_to_quad(quadrilateral_t a, quadrilateral_t b) {
    return quad_to_square(a) * square_to_quad(b);
}


#endif//PERSPECTIVE_H_
