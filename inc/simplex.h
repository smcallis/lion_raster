/*******************************************************************************
 * Copyright (c) 2012 Sean McAllister
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
    
#ifndef SIMPLEX_H_
#define SIMPLEX_H_

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/cursorfont.h>

// M_SQRT2 is not ANSI
#ifndef M_SQRT2
#define M_SQRT2 1.41421356237309504880
#endif

// coordinate type for rectangle/points
typedef double coord_t;

//------------------------------------------------------------------------------
// Enumerations and data type definitions
//------------------------------------------------------------------------------

// rectangle/point structures for both integer and floating point
typedef struct { coord_t x0,y0,x1,y1; } rect_t;
typedef struct { coord_t x,y;         } point_t;


// Event types
typedef enum {
    WINDOW_RESIZE, WINDOW_CLOSE, WINDOW_MOVE, 

    MOUSE_ENTER,   MOUSE_LEAVE,  MOUSE_MOVE,
    MOUSE_DOWN,    MOUSE_DRAG,   MOUSE_UP,

    KEY_PRESS
} evt_type_t;


// Event structure, holds event type and information about it
typedef struct {
    evt_type_t type;
    int c,m,x,y,w,h;

    unsigned mod; // Any modifiers for a key press    
    KeySym   sym; // Raw KeySym
} event_t;


// Basic pixel type, 32-bits, 8 bits per field, plus alpha
typedef struct pixel_t {
    uint8_t b,g,r,a;  // Alpha is a 0.8 fixed point opacity value
} pixel_t; 


// Pixel buffer type
typedef struct pixbuf_t pixbuf_t;
struct pixbuf_t {
    pixel_t      *pixels;
    uint16_t      width;
    uint16_t      height;
    uint16_t      stride;
    bool          blend;       // enable/disable alpha blending   
    rect_t        cliprect;    // rectangle for clipping when using built-in drawing routines

    int16_t       x,y;         // top left of pixbuffer in global coordinates

    rect_t       *dirty;       // list of dirty regions in the pixbuffer
    size_t        dirty_size;  // capacity of dirty list
    size_t        ndirty;      // number of dirty rectangles

    pixbuf_t     *parent;      // parent of this pixbuf
    pixbuf_t    **children;    // list of child pixbuffers to blend on top of this one.  Their coordinates are relative to parent.
    size_t        csize;       // size of child list
    size_t        nchild;      // number of children

    bool          enabled;     // indicates whether the pixbuffer should be drawn.
};


// Represents X11-window, redefine annoying X11 types to make things shorter
#define XWIN_MAX_EXPOSE 256

typedef XSetWindowAttributes x_win_attrs;
typedef struct {
    // Variables for X11 connection
    char       *host;           // Hostname of X11 server
    Display    *dpy;            // Display to draw to
    int         screen;         // Screen to draw to

    // X11 handles/attributes for this window
    Window      parent;         // (Optional) handle to window's parent
    Window      window;         // Handle for this window
    x_win_attrs attr;           // Window attributes
    GC          gc;             // Graphics context
    uint16_t    width, height;  // Width and height of window
    int16_t     left,  top;     // Top/left position of window (relative to parent) 

    // Variables for pixmap construction and maintenance
    pixbuf_t    blendbuf;       // Pixbuf that layers are blended into
    void       *pm_buffer;      // Generic memory buffer for constructing pixmap
    uint16_t    pm_w, pm_h;     // Pixmap width and height
    Pixmap      pixmap;         // X11 pixmap object, constructed from memory in pm_buffer
    
    // To support dragging
    int         mouse;          // When dragging, contains mouse button number being held
    int         down_x, down_y; // When dragging, contains start coordinates

    // Event aggregation
    int         resized;        // Was the window resized?  If so then new size is in following:
    int         window_w, window_h;

    int         mouse_moved;    // Was the mouse moved?  If so then new coordinates are in:
    int         mouse_x,  mouse_y;

    rect_t      expose[XWIN_MAX_EXPOSE];
    size_t      nexpose;        // number of expose events pending
    
    // Drawing related variables
    uint8_t     gamma[256];     // Gamma correction curve
    uint8_t     palette[216];   // Netscape colormap for 8-bits

    // Atoms for interating with window manager
    Atom        proto;
    Atom        close;

    // Color depth conversion tables
    void      *r_cdtable, *g_cdtable, *b_cdtable;
} xwin_t;



//------------------------------------------------------------------------------
// Prelude functions that are integral across sections
//------------------------------------------------------------------------------

// Ansi-C doesn't include strdup
static inline char* dm_strdup(const char *str) {
    if (str == NULL) {
        return NULL;
    }

    char *dup = (char*)malloc(strlen(str) + 1);
    if (dup){
        strcpy(dup, str);
    }
    return dup;
}


// return true if a pixel is null (all zeros)
static inline bool null_color(pixel_t pix) {
    return \
        pix.r == 0 &&
        pix.g == 0 &&
        pix.b == 0 &&
        pix.a == 0;
}

static inline pixel_t rgba_pixel(uint8_t rr, uint8_t gg, uint8_t bb, uint8_t aa) {
    pixel_t ans;
    ans.r = ((uint16_t)rr*aa)/255;
    ans.g = ((uint16_t)gg*aa)/255;
    ans.b = ((uint16_t)bb*aa)/255;
    ans.a = aa;
    return  ans;
}

static inline pixel_t rgb_pixel (uint8_t  rr, uint8_t gg, uint8_t bb) { return rgba_pixel(rr, gg, bb, 255);                                                           }
static inline pixel_t rgba_color(uint32_t rgba)                       { return rgba_pixel((rgba >> 24) & 0xFF, (rgba >> 16) & 0xFF, (rgba >> 8) & 0xFF, rgba & 0xFF); }
static inline pixel_t rgb_color (uint32_t rgb)                        { return rgba_color(rgb << 8 | 0xFF);                                                           }

// set non pre-multiplied pixel value's alpha
static inline pixel_t pixel_set_alpha  (pixel_t pixel, uint8_t alpha) { return rgba_pixel(pixel.r, pixel.g, pixel.b, alpha);                                          }

// undo alpha pre-multiplication to get base color
static inline pixel_t pixel_unset_alpha(pixel_t pixel) {
    pixel_t ans;
    ans.a = pixel.a;
    ans.r = ((uint16_t)pixel.r * 255)/pixel.a;
    ans.g = ((uint16_t)pixel.g * 255)/pixel.a;
    ans.b = ((uint16_t)pixel.b * 255)/pixel.a;
    return ans;
}


/*******************************************************************************
 * Function to blend a source pixel on top of a destination pixel.  Presumes
 * pre-multiplied alpha in each of the two colors.
 *
 * @param dst  destination color
 * @param src  source color
 *
 * @return blended color
 *******************************************************************************/
static inline pixel_t blend_pixel(pixel_t dst, pixel_t src) {
    pixel_t pixel;
    pixel.r = src.r + (((uint16_t)dst.r*(255-src.a)) >> 8);
    pixel.g = src.g + (((uint16_t)dst.g*(255-src.a)) >> 8);
    pixel.b = src.b + (((uint16_t)dst.b*(255-src.a)) >> 8);
    pixel.a = src.a + (((uint16_t)dst.a*(255-src.a)) >> 8);

    return pixel;
}


// Prototypes when needed;
static inline rect_t  rectangle  (coord_t x0, coord_t y0, coord_t width, coord_t height);
static inline point_t point      (coord_t x,  coord_t y);
static inline rect_t  rect_isect (rect_t rect1, rect_t rect2);
static inline bool    rect_empty (rect_t rect);
static inline coord_t rect_width (rect_t rect);
static inline coord_t rect_height(rect_t rect);
static inline bool    rect_equal (rect_t recta, rect_t rectb);



//--------------------------------------------------------------------------------
// Pixbuf related functions
//--------------------------------------------------------------------------------

/*******************************************************************************
 * Resize memory associated with pixbuffer.  This also clears the memory to zero
 * and resets any clipping rectangle to the entire pixbuffer dimensions.
 *
 * @param pb      pointer to pixbuf_t instance to initialize
 * @param width   width in pixels of pixbuf
 * @param height  height in pixels of pixbuf
 *
 * @return 0 on success, 1 on memory allocation error
 ******************************************************************************/
static inline int pixbuf_resize(pixbuf_t *pb, uint16_t width, uint16_t height) {
    free(pb->pixels);
    pb->pixels = (pixel_t*)calloc(width*height, sizeof(pixel_t));
    if (pb->pixels == NULL) {
        return 1;
    }
    
    pb->stride   = width;
    pb->width    = width;
    pb->height   = height;
    pb->cliprect = rectangle(0,0,width,height);
    
    return 0;
}


/*******************************************************************************
 * Initializes fields of pixbuf_t to default values and does the initial sizing.
 * This should be called before any calls to pixbuf_resize.
 * When creating a pixbuf_t, it may have a non-NULL but invalid pointer for the
 * pixels field, and the first call to realloc in pixbuf_resize will cause a segfault
 * if this function isn't called first to initialize fields.
 *
 * @param pb      pointer to pixbuf_t instance to initialize
 * @param width   width in pixels of pixbuf
 * @param height  height in pixels of pixbuf
 *
 * @return 0 on success, 1 on memory allocation error
 ******************************************************************************/
static inline int pixbuf_init(pixbuf_t *pb, uint16_t width, uint16_t height) {
    pb->parent     = NULL;
    pb->pixels     = NULL;    
    pb->blend      = true;
    pb->enabled    = true;

    pb->x = pb->y  = 0;

    pb->dirty_size = 32;
    pb->dirty      = (rect_t*)malloc(pb->dirty_size*sizeof(rect_t));
    pb->ndirty     = 0;

    pb->csize      = 8;
    pb->children   = (pixbuf_t**)calloc(pb->csize, sizeof(pixbuf_t*));
    pb->nchild     = 0;
    
    return pixbuf_resize(pb, width, height);
}


/*******************************************************************************
 * Free any memory associated with a pixbuf object.
 * It is safe to call pixbuf_resize directly after freeing a pixbuf_t.
 *
 * @param pb  pointer to pixbuf_t instance to free
 *******************************************************************************/
static inline void pixbuf_free(pixbuf_t *pb) {
    free(pb->pixels);
    free(pb->dirty);
    free(pb->children);
    pb->pixels   = NULL;
    pb->dirty    = NULL;
    pb->children = NULL;
}


/*******************************************************************************
 * Enable/disable alpha blending for pixbuf
 *
 * @param pb    pointer to pixbuf_t instance to update
 * @param flag  true for alpha-blending enabled, false to disabled
 *
 * @return current value of alpha-blend flag
 *******************************************************************************/
static inline bool pixbuf_set_blend(pixbuf_t *pb, bool flag) {
    bool curblend = pb->blend;
    pb->blend     = flag;
    return curblend;
}


/*******************************************************************************
 * Set a clipping rectangle on the pixbuf.  Rectangle is clipped to boundaries
 * of the pixbuf.
 *
 * @param pb    pixbuf_t instance to modify
 * @param rect  new rectangle representing clipping region
 *
 * @return old clipping rectangle
 *******************************************************************************/
static inline rect_t pixbuf_clip(pixbuf_t *pb, rect_t rect) {
    rect_t oldclip = pb->cliprect;
    pb->cliprect   = rect_isect(rect, rectangle(0,0,pb->width,pb->height));
    return oldclip;
}


/*******************************************************************************
 * Clear the clipping rectangle, causes further drawing commands to just
 * clip to edges of pixbuf.
 *
 * @param pb    pixbuf_t instance to modify
 *******************************************************************************/
static inline void pixbuf_unclip(pixbuf_t *pb) {
    pb->cliprect = rectangle(0, 0, pb->width, pb->height);
}


/*******************************************************************************
 * Clip an x coordinate for a given pixbuf.  
 *
 * @param pb  pixbuf_t instance to clip in
 * @param xx  x coordinate to clip.
 *******************************************************************************/
static inline uint32_t pixbuf_clip_x(pixbuf_t *pb, int32_t xx) {
    int32_t cx = (xx < pb->cliprect.x0) ? pb->cliprect.x0 : xx;
            cx = (cx > pb->cliprect.x1) ? pb->cliprect.x1 : cx;
    return  cx;
}


/*******************************************************************************
 * Clip a y coordinate for a given pixbuf.
 *
 * @param pb  pixbuf_t instance to clip in
 * @param yy  y coordinate to clip.
 *******************************************************************************/
static inline uint32_t pixbuf_clip_y(pixbuf_t *pb, int32_t yy) {
    int32_t cy = (yy < pb->cliprect.y0) ? pb->cliprect.y0 : yy;
            cy = (cy > pb->cliprect.y1) ? pb->cliprect.y1 : cy;
    return  cy;
}


/********************************************************************************
 * Clears a rectangular region in the pixbuf to a given color.
 *
 * @param pb     pointer to pixbuf_t instance to clear
 * @param color  pixel_t representing color to clear to
 * @param rect   rectangular region to clear
 *******************************************************************************/
static inline void pixbuf_clear_rect(pixbuf_t *pb, pixel_t color, rect_t rect) {
    uint32_t xl = pixbuf_clip_x(pb, rect.x0),   xh = pixbuf_clip_x(pb, rect.x1);
    uint32_t yl = pixbuf_clip_y(pb, rect.y0),   yh = pixbuf_clip_y(pb, rect.y1);

    for (uint32_t yy=yl; yy < yh; yy++) {
        pixel_t *pix = pb->pixels + yy*pb->stride + xl;

        for (uint32_t xx=xl; xx < xh; xx++) {
            *pix++ = color;
        }
    }
}


/********************************************************************************
 * Convenience function to clear entire pixbuf to given color
 *
 * @param pb    pixbuf_t instance to clear
 * @param color pixel_t instance representing color to clear to
 ********************************************************************************/
static inline void pixbuf_clear (pixbuf_t *pb, pixel_t color) {
    if (null_color(color)) {
        memset(pb->pixels, 0, pb->stride*pb->height*sizeof(pixel_t));
    } else {
        pixbuf_clear_rect(pb, color, rectangle(0, 0, pb->width, pb->height));
    }
}


/*******************************************************************************
 * Mark a rectangular region as dirty in the pixbuffer
 *
 * @param pb    pixbuf_t instance to modify
 * @param rect  rect_T representing the dirty region
 *******************************************************************************/
static inline void pixbuf_redraw (pixbuf_t *pb, rect_t region) {
    if (pb->ndirty == pb->dirty_size) {
        pb->dirty_size *= 2;
        pb->dirty       = (rect_t*)realloc(pb->dirty, pb->dirty_size*sizeof(rect_t));
    }

    // intersect pixbuf region with dirty rectangle and store result
    int16_t pbx = (pb->parent) ? pb->x : 0;
    int16_t pby = (pb->parent) ? pb->y : 0;
    pb->dirty[pb->ndirty++] = rect_isect(region, rectangle(pbx, pby, pb->width, pb->height));
}


/*******************************************************************************
 * Add a pixbuffer as a child to another pixbuffer.  Children are composed on top
 * of their parent.
 *
 * @param pb     pointer to parent pixbuffer
 * @param child  pointer to child pixbuffer
 *******************************************************************************/
static inline void pixbuf_add_child(pixbuf_t *pb, pixbuf_t *child) {
    if (pb->nchild == pb->csize) {
        size_t oldsize = pb->csize;
        pb->csize     *= 2; 
        pb->children   = (pixbuf_t**)realloc(pb->children, pb->csize*sizeof(pixbuf_t*));

        // ensure new children are NULL
        for (size_t ii=oldsize; ii < pb->csize; ii++) {
            pb->children[ii] = NULL;
        }
    }

    child->parent              = pb;
    pb->children[pb->nchild++] = child;
}


/*******************************************************************************
 * Remove a child pixbuffer from another pixbuf.  Does nothing is pixbuffer
 * isn't a child.
 *
 * @param pb     pointer to parent pixbuffer
 * @param child  pointer to child pixbuffer to remove
 *******************************************************************************/
static inline void pixbuf_del_child(pixbuf_t *pb, pixbuf_t *child) {
    for (size_t ii=0; ii < pb->nchild; ii++) {
        if (pb->children[ii] == child) {
            // shift other children down
            for (size_t jj=ii; jj < pb->nchild-1; jj++) {
                pb->children[jj] = pb->children[jj+1];
            }
            pb->children[pb->nchild-1] = NULL;
            pb->nchild--;
            break;
        }
    }
    child->parent = NULL;
}


/*******************************************************************************
 * Unconditionally clear the dirty list for a pixbuffer.
 *
 * @param pb  pixbuf_t to clear
 *******************************************************************************/
static inline void pixbuf_clear_dirty(pixbuf_t *pb) {
    pb->ndirty = 0;
}


// recursive helper to merge dirty lists
static inline size_t merge_dirty_(pixbuf_t *parent, rect_t **list, size_t used, size_t *size) {
    // store dirty rectangles from parent
    for (size_t ii=0; ii < parent->ndirty; ii++) {
        if (used == *size) {
            *size *= 2;
            *list  = (rect_t*)realloc(*list, *size*sizeof(rect_t));
        }

        (*list)[used++] = parent->dirty[ii];
    }

    // and for each child
    for (size_t ii=0; ii < parent->nchild; ii++) {
        used = merge_dirty_(parent->children[ii], list, used, size);
    }

    pixbuf_clear_dirty(parent);    

    return used;
}


// recursive helper to blend children into target
static inline void blend_children_(pixbuf_t *target, pixbuf_t *source, rect_t region) {
    if (!source->enabled) {
        return;
    }

    rect_t irect = rect_isect(
        rectangle(source->x, source->y, source->width, source->height), region);

    pixel_t *dst = &target->pixels[(size_t)( irect.y0*target->stride+irect.x0)];
    pixel_t *src = &source->pixels[(size_t)((irect.y0-source->y)* source->stride+(irect.x0-source->x))];
    for (size_t row=0; row < rect_height(irect); row++) {
        for (size_t col=0; col < rect_width(irect); col++) {
            dst[col] = blend_pixel(dst[col], src[col]);
        }
        dst += target->stride;
        src += source->stride;
    }

    // blend children on top
    for (size_t ii=0; ii < source->nchild; ii++) {
        blend_children_(target, source->children[ii], region);
    }
}


/*******************************************************************************
 * Compose a pixbuffer and its children into another pixbuffer, which is then
 * suitable for blitting to the screen.  This is done by merging the dirty lists
 * of the pixbuffer and its children.  Each dirty region is then recomposed to
 * the target pixbuffer.
 *
 * After composing, a pointer to a list of regions that need to be redrawn to the
 * screen is returned and the dirty lists for the pixbuf and children are cleared.
 *
 * @param target  pixbuffer to compose into
 * @param source  pixbuffer to composite
 *******************************************************************************/
static inline rect_t* pixbuf_composite(pixbuf_t *target, pixbuf_t *source, size_t *ndirty) {
    static rect_t *redraw_list = NULL;
    static size_t  redraw_size = 0;
    
    if (redraw_list == NULL) {
        redraw_size = 16;
        redraw_list = (rect_t*)malloc(redraw_size*sizeof(rect_t));
    }
    
    *ndirty = merge_dirty_(source, &redraw_list, 0, &redraw_size);

    for (size_t ii=0; ii < *ndirty; ii++) {
        rect_t region = rect_isect(rectangle(0, 0, target->width, target->height),
                                   redraw_list[ii]);
        
        if (rect_empty(region)) {
            continue;
        }

        // Copy background to pixmap
        pixel_t *dst = &target->pixels[(size_t)(region.y0*target->stride+region.x0)];
        pixel_t *src = &source->pixels[(size_t)(region.y0*source->stride+region.x0)];
        for (size_t row=0; row < rect_height(region); row++) {
            for (size_t col=0; col < rect_width(region); col++) {
                dst[col] = src[col];
            }
            dst += target->stride;
            src += source->stride;
        }

        for (size_t cc=0; cc < source->nchild; cc++) {
            blend_children_(target, source->children[cc], region);
        }
    }
    
    return redraw_list;
}


/*******************************************************************************
 * Copy a region from one pixbuffer to another.  No blending is done
 *
 * @param src       source pixbuffer to copy from
 * @param src_rect  rectangle representing region to copy
 * @param dst       destination pixbfuffer
 * @param x         x coordinate in destination to draw to
 * @param y         y coordinate in destination to draw to
 *******************************************************************************/
static inline void pixbuf_copy_rect(pixbuf_t *src, rect_t src_rect, pixbuf_t *dst, ssize_t xx, ssize_t yy) {
    rect_t dst_rect = rectangle(xx, yy, rect_width(src_rect), rect_height(src_rect));
    
    // move top/left corner of copy area if needed
    if (dst_rect.x0 < 0) { src_rect.x0 += abs(dst_rect.x0);  dst_rect.x0  = 0; }
    if (dst_rect.y0 < 0) { src_rect.y0 += abs(dst_rect.y0);  dst_rect.y0  = 0; }
    if (src_rect.x0 < 0) { dst_rect.x0 += abs(src_rect.x0);  src_rect.x0  = 0; }
    if (src_rect.y0 < 0) { dst_rect.y0 += abs(src_rect.y0);  src_rect.y0  = 0; }
    
    // move bottom/right corner of copy area if needed
    if (dst_rect.x1 > dst->cliprect.x1 ) { src_rect.x1 += dst->cliprect.x1-dst_rect.x1;  dst_rect.x1 = dst->cliprect.x1;  }
    if (dst_rect.y1 > dst->cliprect.y1)  { src_rect.y1 += dst->cliprect.y1-dst_rect.y1;  dst_rect.y1 = dst->cliprect.y1; }
    if (src_rect.x1 > src->width )       { dst_rect.x1 += src->width      -src_rect.x1;  src_rect.x1 = src->width;  }
    if (src_rect.y1 > src->height)       { dst_rect.y1 += src->height     -src_rect.y1;  src_rect.y1 = src->height; }

    if (rect_empty(src_rect) || rect_empty(dst_rect)) {
        return;
    }
    
    // rectangles should be the same size now, so do the copy
    for (ssize_t yy=0; yy < rect_height(src_rect); yy++) {
        pixel_t *sptr = &src->pixels[(size_t)((yy + src_rect.y0)*src->stride + src_rect.x0)];
        pixel_t *dptr = &dst->pixels[(size_t)((yy + dst_rect.y0)*dst->stride + dst_rect.x0)];
        
        memcpy(dptr, sptr, (size_t)rect_width(src_rect)*sizeof(pixel_t));
    }
}


//--------------------------------------------------------------------------------
// Rectangle functions
//--------------------------------------------------------------------------------

// dimension/query functions for integer rectangles
static inline coord_t rect_width  (rect_t rect) { return rect.x1-rect.x0; }
static inline coord_t rect_height (rect_t rect) { return rect.y1-rect.y0; }
static inline bool    rect_empty  (rect_t rect) { return (rect_width(rect) <= 0) || (rect_height(rect) <= 0); }
static inline bool    rect_equal  (rect_t recta, rect_t rectb) {
    return \
        (recta.x0 == rectb.x0) &&
        (recta.y0 == rectb.y0) &&
        (recta.x1 == rectb.x1) &&
        (recta.y1 == rectb.y1);        
}

static inline point_t point        (coord_t xx, coord_t yy)             { point_t pnt = {xx,yy}; return pnt; }
static inline bool    in_range     (coord_t xx, coord_t ll, coord_t hh) { return (ll <= xx) && (xx < hh);    } 
static inline bool    point_in_rect(coord_t xx, coord_t yy, rect_t rect) {
    return in_range(xx, rect.x0, rect.x1) && in_range(yy, rect.y0, rect.y1);
}


/*******************************************************************************
 * Create a new integer rectangle with the given coordinates/dimensions.
 *
 * @param x0      x coordinate of top,left corner of rectangle.
 * @param y0      y coordinate of top,left corner of rectangle.
 * @param width   width of rectangle
 * @param height  height of rectangle
 *
 * @return new rect_t instance representing rectangle
 *******************************************************************************/
static inline rect_t rectangle(coord_t x0, coord_t y0, coord_t width, coord_t height) {
    rect_t ret = {x0, y0, x0 + width, y0 + height};
    return ret;
}


/*******************************************************************************
 * Create a rectangle from two points.
 *
 * @param pnt0  first point
 * @param pnt1  second point
 *
 * @return new rect_t instance from points
 *******************************************************************************/
static inline rect_t rectanglep(point_t pnt0, point_t pnt1) {
    rect_t rect;
    rect.x0 = fmin(pnt0.x, pnt1.x);
    rect.y0 = fmin(pnt0.y, pnt1.y);
    rect.x1 = fmax(pnt0.x, pnt1.x);
    rect.y1 = fmax(pnt0.y, pnt1.y);
    return rect;
}


/*******************************************************************************
 * Return rectangle representing the intersection of two integer rectangles.
 *
 * @param rect1  first rectangle to instersect
 * @param rect2  second rectangle to instersect
 *
 * @return rectangle representing intersection between rect1 and rect2
 *******************************************************************************/
static inline rect_t rect_isect (rect_t rect1, rect_t rect2) {
    rect_t ret = {0,0,0,0};

    // If resulting intersection would be empty, just return an empty rectangle 
    if ((rect1.x1 < rect2.x0)  ||  // rect1 fully left of rect2
        (rect2.x1 < rect1.x0)  ||  // rect2 fully left of rect1
        (rect1.y1 < rect2.y0)  ||  // rect1 full above rect2
        (rect2.y1 < rect1.y0)) {   // rect2 full above rect1
        return ret;
    }
    
    // Take min/max of dimensions as appropriate.
    ret.y0 = (rect1.y0 > rect2.y0) ? rect1.y0 : rect2.y0;
    ret.x0 = (rect1.x0 > rect2.x0) ? rect1.x0 : rect2.x0;
    ret.y1 = (rect1.y1 < rect2.y1) ? rect1.y1 : rect2.y1;
    ret.x1 = (rect1.x1 < rect2.x1) ? rect1.x1 : rect2.x1;
    return ret;
}


/*******************************************************************************
 * Round a rectangle to integer coordinates by calling floor() on lower coordinates
 * and ceil() on upper coordinates.  This ensures the rectangle never shrinks
 *
 * @param rect  rectangle to round
 *
 * @return rounded rectangle
 *******************************************************************************/
static inline rect_t rect_round(rect_t rect) {
    rect.x0 = floor(rect.x0);  rect.y0 = floor(rect.y0);  
    rect.x1 =  ceil(rect.x1);  rect.y1 =  ceil(rect.y1);  
    return rect;
}


/*******************************************************************************
 * Return bounding box rectangle for two rectangles
 *
 * @param rect1  first rectangle
 * @param rect2  second rectangle
 *
 * @return rectangle representing bounding box around rectangles
 *******************************************************************************/
static inline rect_t rect_bbox (rect_t rect1, rect_t rect2) {
    return rectanglep(
        point(fmin(rect1.x0, rect2.x0),
              fmin(rect1.y0, rect2.y0)),
        point(fmax(rect1.x1, rect2.x1),
              fmax(rect1.y1, rect2.y1)));
}


/*******************************************************************************
 * Shrink a rectangle by reducing it by a certain amount in each direction
 *
 * @param rect   rectangle to shrink
 * @param delta  delta by which to change rectangle
 *
 * @return shrunken rectangle
 *******************************************************************************/
static inline rect_t rect_shrink(rect_t rect, coord_t delta) {
    return rectanglep(
        point(fmin(rect.x1, rect.x0+delta), fmin(rect.y1, rect.y0+delta)),
        point(fmax(rect.x0, rect.x1-delta), fmax(rect.y0, rect.y1-delta)));
}


/*******************************************************************************
 * Grow a rectangle by expanding it by a certain amount in each direction
 *
 * @param rect   rectangle to grow
 * @param delta  delta by which to change rectangle
 *
 * @return expanded rectangle
 *******************************************************************************/
static inline rect_t rect_grow(rect_t rect, coord_t delta) {
    return rectanglep(
        point(rect.x0-delta, rect.y0-delta),
        point(rect.x1+delta, rect.y1+delta));
}


/*******************************************************************************
 * Assumes that two rectangles represent the same region and maps a point in
 * the first into a point in the second.
 *
 * @param src  source rectangle
 * @param dst  destination rectangle
 * @param pnt  point in source coordinates
 *
 * @return point in destination coordinates
 *******************************************************************************/
static inline point_t map_point(rect_t src, rect_t dst, point_t spnt) {
    return point(    
        dst.x0 + (spnt.x - src.x0) * rect_width (dst)/rect_width (src),
        dst.y0 + (spnt.y - src.y0) * rect_height(dst)/rect_height(src));
}


/*******************************************************************************
 * Map a rectangular region from one coordinate system to another.
 *
 * @param src   source region
 * @param dst   destination region
 * @param rect  rectangle to map
 *
 * @return rectangle in new coordinates
 *******************************************************************************/
static inline rect_t map_rect(rect_t src, rect_t dst, rect_t rect) {
    return rectanglep(
               map_point(src, dst, point(rect.x0, rect.y0)),
               map_point(src, dst, point(rect.x1, rect.y1)));
}


/*******************************************************************************
 * Translate a point to a new origin
 *
 * @param src  point_t representing source origin
 * @param dst  point_t representing destination origin
 * @param pnt  point_t to translate
 *
 * @return translated point
 *******************************************************************************/
static inline point_t translate_point(point_t srco, point_t dsto, point_t pnt) {
    return point(pnt.x + srco.x - dsto.x, pnt.y + srco.y - dsto.y);    
}


/*******************************************************************************
 * Translate a rectangle to a new origin
 *
 * @param src  point_t representing source origin
 * @param dst  point_t representing destination origin
 * @param rect rect_t to translate
 *
 * @return translated rectangle
 *******************************************************************************/
static inline rect_t translate_rect(point_t srco, point_t dsto, rect_t rect) {
    rect_t ans = rect;
    ans.x0 += srco.x - dsto.x;
    ans.x1 += srco.x - dsto.x;
    ans.y0 += srco.y - dsto.y;
    ans.y1 += srco.y - dsto.y;
    return ans;
}


//--------------------------------------------------------------------------------
// X11 Interface
//--------------------------------------------------------------------------------

// Event callback function pointer
typedef int (*handler_t)(xwin_t*, event_t*);


/********************************************************************************
 * Main event dispatch function.  Should be called repeatedly in an inner
 * loop to handle window events.
 *
 * @param xwin  pointer to xwin_t instance to poll events for
 * @param evt   pointer to event_t instance that will receive event information
 *
 * @return true if event is ready, with information in evt, false otherwise.
 ********************************************************************************/
static inline bool xwin_event(xwin_t *xwin, event_t *evt) {
    // if we get here with queued up expose events and we're not resizing
    // then we just emitted a resize event so now we have real data to send to the screen
    if (xwin->resized == 0 && xwin->nexpose > 0) {
        XLockDisplay  (xwin->dpy);
        for (size_t ii=0; ii < xwin->nexpose; ii++) {
            rect_t rect = xwin->expose[ii];
            XCopyArea(xwin->dpy, xwin->pixmap, xwin->window, xwin->gc,
                (size_t)rect.x0, (size_t)rect.y0, (size_t)rect_width(rect), (size_t)rect_height(rect), (size_t)rect.x0, (size_t)rect.y0);
        }
        xwin->nexpose=0;
        XUnlockDisplay(xwin->dpy);
    }

    
    // loop until we return or no more events are available
    while (true) {
        XLockDisplay(xwin->dpy);
        if (!XPending(xwin->dpy)) {
            XUnlockDisplay(xwin->dpy);
            break; // no more events in queue, break out
        }

        // pull event off the queue
        XEvent event;
        XNextEvent    (xwin->dpy, &event);
        XUnlockDisplay(xwin->dpy);

        switch(event.type) {

        // Configuration events such as resize, move, etc
        case ConfigureNotify: {
            int xx = event.xconfigure.x;
            int yy = event.xconfigure.y;            
            int ww = event.xconfigure.width;
            int hh = event.xconfigure.height;            

            // have to convert coordinates to be absolute relative to the root to avoid issues
            // with window manager reparenting
            Window root, parent, *children; unsigned nchild;
            XQueryTree(xwin->dpy, xwin->window, &root, &parent, &children, &nchild);
            if (!XTranslateCoordinates(xwin->dpy, xwin->window, root, 0, 0, &xx, &yy, &root)) {
                xx = 0; // best we can do
                yy = 0; 
            }
            
            if ((xwin->width != ww) || (xwin->height != hh)) {
                // note that the resize occurred, but don't return event until we've emptied the queue
                xwin->left     = xx;
                xwin->top      = yy;
                xwin->window_w = ww;
                xwin->window_h = hh;
                xwin->resized += 1;
            } else {
                // only moved, no change in geometry
                if ((xwin->left != xx) || (xwin->top != yy)) {
                    xwin->left  = xx;
                    xwin->top   = yy;

                    evt->type = WINDOW_MOVE;
                    evt->x    = xx;
                    evt->y    = yy;
                    
                    return true;
                }
            }
            
            break;
        }

        // Part of the window was revealed, copy data there.
        case Expose: {
            if (xwin->pixmap) {
                int xx = event.xexpose.x;
                int yy = event.xexpose.y;
                int ww = event.xexpose.width;
                int hh = event.xexpose.height;
                
                // if a resize is pending, defer exposes until afterwards
                if (xwin->resized) {
                    if (xwin->nexpose < XWIN_MAX_EXPOSE) {
                        xwin->expose[xwin->nexpose++] = rectangle(xx, yy, ww, hh);
                    }
                } else {
                    XLockDisplay  (xwin->dpy);
                    XCopyArea(xwin->dpy, xwin->pixmap, xwin->window, xwin->gc,
                        xx, yy, ww, hh, xx, yy);
                    XUnlockDisplay(xwin->dpy);
                }
            }
            break;
        }

        // ClientMessage is used to intercept window manager close events
        case ClientMessage: {
            Atom type = event.xclient.message_type;
            Atom msg  = event.xclient.data.l[0];
            if ((type == xwin->proto) && (msg == xwin->close)) {
                evt->type = WINDOW_CLOSE;
                return true;
            }
            break;
        }

        // Notify of key presses
        case KeyPress: {
            char           buf[2];
            KeySym         sym;
            XLockDisplay(xwin->dpy);
            {
                if (XLookupString(&event.xkey, buf, 2, &sym, NULL) == 0) {
                    buf[0] = 0;
                }
            }
            XUnlockDisplay(xwin->dpy);

            evt->type = KEY_PRESS;
            evt->c    = buf[0];
            evt->mod  = event.xkey.state;
            evt->sym  = sym;
            evt->x    = event.xkey.x;
            evt->y    = event.xkey.y;
            return true;
        }

        // Notify of mouse motion
        case MotionNotify: {
            // record event but don't return event until queue is empty
            xwin->mouse_x = event.xmotion.x;
            xwin->mouse_y = event.xmotion.y;
            xwin->mouse_moved = true;
            break;
        }

        // Notify of mouse enter 
        case EnterNotify: {
            evt->type = MOUSE_ENTER;
            evt->x    = event.xcrossing.x;
            evt->y    = event.xcrossing.y;
            return true;
        }

        // Notify of mouse exit
        case LeaveNotify: {
            evt->type = MOUSE_LEAVE;
            evt->x    = event.xcrossing.x;
            evt->y    = event.xcrossing.y;
            return true;
        }

        // Notify of mouse down event
        case ButtonPress: {
            evt->type = MOUSE_DOWN;
            evt->x    = event.xbutton.x;
            evt->y    = event.xbutton.y;
            evt->m    = event.xbutton.button;

            if (!xwin->mouse) {
                xwin->mouse  = evt->m;
                xwin->down_x = evt->x;
                xwin->down_y = evt->y;
            }

            return true;
        }

        // Notify of mouse up event
        case ButtonRelease: {
            evt->type   = MOUSE_UP;
            evt->x      = event.xbutton.x;
            evt->y      = event.xbutton.y;
            evt->m      = event.xbutton.button;
            xwin->mouse = 0;

            return true;
        }

        default:
            break;
        }
    } // while (true)


    // check for window resized
    if (xwin->resized > 0) {
        xwin->resized = 0;

        int16_t ww = xwin->window_w;
        int16_t hh = xwin->window_h;
        
        // avoid zero dimensions
        ww = ww ? ww : 1;
        hh = hh ? hh : 1;
            
        if (ww > xwin->pm_w || hh > xwin->pm_h) { 
            if (ww > xwin->pm_w) xwin->pm_w = ww + 250;
            if (hh > xwin->pm_h) xwin->pm_h = hh + 250;

            // reallocate pixmap
            XLockDisplay(xwin->dpy);
            {
                if (xwin->pixmap) {
                    XFreePixmap(xwin->dpy, xwin->pixmap);
                }
                
                xwin->pixmap = XCreatePixmap(xwin->dpy, xwin->window, xwin->pm_w, xwin->pm_h, 
                    DefaultDepth(xwin->dpy, xwin->screen));
            }
            XUnlockDisplay(xwin->dpy);
            
            // allocate buffer for creating pixmap images
            xwin->pm_buffer = realloc(xwin->pm_buffer, xwin->pm_w*xwin->pm_h*sizeof(uint32_t));
            
            // Resize blendbuf
            pixbuf_resize(&xwin->blendbuf, xwin->pm_w, xwin->pm_h);
        }
            
        // emit WINDOW_RESIZE event
        xwin->width  = ww;
        xwin->height = hh;
        evt->type    = WINDOW_RESIZE;
        evt->w       = ww;
        evt->h       = hh;
        evt->x       = xwin->left;
        evt->y       = xwin->top;
        return true;
    }

    // check for mouse moved
    if (xwin->mouse_moved) {
        xwin->mouse_moved = false;
        evt->x = xwin->mouse_x;
        evt->y = xwin->mouse_y;

        if (xwin->mouse) {
            evt->type = MOUSE_DRAG;
            evt->w    = evt->x - xwin->down_x;
            evt->h    = evt->y - xwin->down_y;
            evt->m    = xwin->mouse;
        } else {            
            evt->type = MOUSE_MOVE;
        }

        return true;
    }

    // no events in queue or aggregated
    return false;
}


/********************************************************************************
 * Generates a single table of color conversion values.  The resulting table is
 * suitable for converting 8-bit color values to an n-bit masked and shifted
 * color value suitable for or-ing with the other color fields to produce a final
 * color value.
 *
 * @param xwin   pointer to xwin containing gamma correction table to use
 * @param table  pointer to table pointer we want to populate.  This function will allocate
 *               memory for this table using realloc and the memory should be freed.
 * @param depth  bit-depth that we're converting to, determine bytes per value in the table
 * @param mask   color mask to use for shifting generated values
 *
 ********************************************************************************/
static inline void xwin_gen_cdtable(xwin_t *xwin, void** table, uint32_t depth, uint32_t mask) {
    uint8_t mshift=0;  while ((mask & 0x1) == 0) { mshift++; mask >>= 1; } // bit position of mask bits
    uint8_t sshift=8;  while ((mask & 0x1))      { sshift--; mask >>= 1; } // how many bits to shift down to truncate to mask length
    
    uint8_t nbytes=0;
    if (depth > 32) {
        fprintf(stderr, "Don't know how to handle bit depth of %i\n", depth);
        exit(EXIT_FAILURE);
    } else if (depth > 16) {
        nbytes = 4;
    } else if (depth >  8) {
        nbytes = 2;
    } else {
        nbytes = 1;
    }

    // Allocate memory for the field
    *table = realloc(*table, nbytes*256);

    // Generate table, have to have a branch for each bit depth, so it goes.
    switch(nbytes) {
    case 4: {
        uint32_t *ptr = (uint32_t*)*table;
        for (uint16_t ii=0; ii < 256; ii++) { *ptr++ = xwin->gamma[ii] >> sshift << mshift; }
        break;
    }
    case 2: {
        uint16_t *ptr = (uint16_t*)*table;
        for (uint16_t ii=0; ii < 256; ii++) { *ptr++ = xwin->gamma[ii] >> sshift << mshift; }
        break;
    }
    case 1: {
        uint8_t *ptr = (uint8_t*)*table;
        for (uint16_t ii=0; ii < 256; ii++) { *ptr++ = xwin->gamma[ii] >> sshift << mshift; }
        break;
    }
    default:
        fprintf(stderr, "Error, don't know how to handle %i bytes/field color depth\n", nbytes);
        exit(EXIT_FAILURE);
    }
}


/********************************************************************************
 * Regenerate color-depth conversion tables for given window.  These are used
 * at run time to do fast bit depth conversion for displaying on screen.  They also
 * include appropriate gamma correction so that doesn't need to be done dynamically.
 *
 * @param xwin  pointer to xwin_t instance that should have its tables updated
 *
 ********************************************************************************/
static inline void xwin_gen_cdtables(xwin_t *xwin) {
    // Get color depth
    uint8_t  depth  = DefaultDepth (xwin->dpy, xwin->screen);
    Visual*  visual = DefaultVisual(xwin->dpy, xwin->screen);

    // Generate table for each color field
    xwin_gen_cdtable(xwin, &xwin->r_cdtable, depth, visual->red_mask);
    xwin_gen_cdtable(xwin, &xwin->g_cdtable, depth, visual->green_mask);
    xwin_gen_cdtable(xwin, &xwin->b_cdtable, depth, visual->blue_mask);
}


/*******************************************************************************
 * Generate gamma correction curve for give window.  Regenerates the gamma table
 * and then updates the color-conversion tables to take it into account.
 *
 * @param xwin   pointer to xwin_t instance to change the gamme correction of
 * @param gamma  new gamma correction value
 *******************************************************************************/
static inline void xwin_gamma(xwin_t *xwin, double gamma) {
    for (uint16_t ii=0; ii < 256; ii++) {
        xwin->gamma[ii] = (uint8_t)(0.5 + 255.0*pow(ii/255.0, gamma));
    }

    // Regenerate color-depth conversion tables
    xwin_gen_cdtables(xwin);
}


/*******************************************************************************
 * Create a new window with the given parent, coordinates, and dimensions.
 *
 * @param xwin    pointer to xwin_t instance to initialize
 * @param parent  0 to create a child of the root window, else a valid X11 id
 * @param width   width of window in pixels
 * @param height  height of window in pixels
 * @param left    coordinate of left side of window
 * @param top     coordinate of top of window
 *
 * @return 0 on success, 1 on error.
 *******************************************************************************/
static inline int xwin_init(xwin_t *xwin, Window parent,
                             int    width, int   height,
                             int    left,  int   top)  {

    memset(xwin, 0, sizeof(xwin_t));
    
    // Open connection to X server
    xwin->dpy  = XOpenDisplay(NULL);
    xwin->host = dm_strdup(getenv("DISPLAY"));
    if (xwin->dpy == NULL) {
        fprintf(stderr, "xiface -- couldn't connect to X server '%s'\n", xwin->host);
        free(xwin->host);
        return 1;
    }
    xwin->screen = DefaultScreen(xwin->dpy);

    // No color-depth conversion tables yet
    xwin->r_cdtable    = NULL;
    xwin->g_cdtable    = NULL;
    xwin->b_cdtable    = NULL;

    // Setup dimensions
    // Don't set width/height yet, let the first resize event do that.
    xwin->mouse  = 0; // Not dragging
    xwin->width  = 0;    xwin->height = 0;
    xwin->left   = left; xwin->top    = top;

    XLockDisplay(xwin->dpy);
    xwin->parent = parent ? parent : RootWindow(xwin->dpy, xwin->screen);

    // Create new window
    long black_pixel = BlackPixel(xwin->dpy, xwin->screen);
    long white_pixel = WhitePixel(xwin->dpy, xwin->screen);

    // Set window attributes
    xwin->attr.border_pixel      = black_pixel;
    xwin->attr.background_pixel  = black_pixel; // Black background
    xwin->attr.background_pixmap = None;        // We'll manage our own backing store
    xwin->attr.backing_store     = NotUseful;
    xwin->attr.save_under        = False;
    xwin->attr.bit_gravity       = StaticGravity;

    // Create window
    xwin->window = XCreateWindow(xwin->dpy, xwin->parent, left, top, width, height,
                                 0, 0, InputOutput, CopyFromParent,
                                 CWBorderPixel | CWBackPixel | CWBackPixmap | CWBackingStore | CWSaveUnder | CWBitGravity,
                                 &xwin->attr);

    // Create pixmap and pm_buffer even though they'll get reinitialized on first resize
    // This will let us draw without entering the event loop
    pixbuf_init(&xwin->blendbuf, width, height);
    xwin->pm_w      = width;
    xwin->pm_h      = height;
    xwin->pixmap    = XCreatePixmap(xwin->dpy, xwin->window, width, height, DefaultDepth(xwin->dpy, xwin->screen));
    xwin->pm_buffer = malloc(width*height*sizeof(uint32_t));
    xwin->nexpose   = 0;
    
    // Set default gamma correction
    xwin_gamma(xwin, 0.7);

    // Build Netscape colormap if we're on 8-bit display
    if (DefaultDepth(xwin->dpy, xwin->screen) <= 8) {
        size_t pidx=0;
        for (int rr=0; rr < 6; rr++) {
            for (int gg=0; gg < 6; gg++) {
                for (int bb = 0; bb < 6; bb++, pidx++) {
                    XColor color; memset(&color, 0, sizeof(XColor));
                    color.flags  = DoRed | DoGreen | DoBlue;
                    color.red    = 0x3333*rr;
                    color.green  = 0x3333*gg;
                    color.blue   = 0x3333*bb;

                    // Try to get color, if we don't, just use previous (assumes black always succeeds)
                    if (XAllocColor(xwin->dpy, DefaultColormap(xwin->dpy, xwin->screen), &color)) {
                        xwin->palette[pidx] = (uint8_t)color.pixel;
                    } else {
                        xwin->palette[pidx] = xwin->palette[pidx - 1];
                    }
                }
            }
        }
    }

    // Create graphics context
    xwin->gc =  XCreateGC(xwin->dpy, xwin->window, 0, 0);
    XSetGraphicsExposures(xwin->dpy, xwin->gc, False);    // No Expose events on Xcopy* functions

    // Define events we want
    XSelectInput(xwin->dpy, xwin->window,
                 KeyPressMask        |
                 ButtonPressMask     | ButtonReleaseMask |
                 EnterWindowMask     | LeaveWindowMask   |
                 PointerMotionMask   | ExposureMask      |
                 StructureNotifyMask);// | SubstructureNotifyMask);

    // Grab some window manager events
    xwin->proto = XInternAtom(xwin->dpy, "WM_PROTOCOLS",     1);
    xwin->close = XInternAtom(xwin->dpy, "WM_DELETE_WINDOW", 0);
    XSetWMProtocols(xwin->dpy, xwin->window, &xwin->close, 1);

    // Clear window and pixmap  
    XSetForeground(xwin->dpy, xwin->gc, BlackPixel(xwin->dpy, xwin->screen));
    XFillRectangle(xwin->dpy, xwin->pixmap, xwin->gc, 0, 0, width, height);
    XFillRectangle(xwin->dpy, xwin->window, xwin->gc, 0, 0, width, height);
    
    // Set foreground color to white by default
    XSetForeground(xwin->dpy, xwin->gc, white_pixel);
    
    // Map to put on screen, send move again in case window manager ignored it
    XMapRaised (xwin->dpy, xwin->window);

    if (left >= 0 || top >= 0) { 
        XMoveWindow(xwin->dpy, xwin->window, left, top);
    }

    // Finally change the cursor
    XDefineCursor(xwin->dpy, xwin->window, XCreateFontCursor(xwin->dpy, XC_tcross));

    XUnlockDisplay(xwin->dpy);
    XSync         (xwin->dpy, false);

    return 0;
}


/*******************************************************************************
 * Free up any memory associated with xwin_t instance.
 *
 * @param xwin  pointer to xwin_t instance to free
 *******************************************************************************/
static inline void xwin_free(xwin_t* xwin) {
    XLockDisplay  (xwin->dpy);
    {
        if (xwin->pixmap) {
            XFreePixmap(xwin->dpy, xwin->pixmap);
        }

        XFreeGC       (xwin->dpy, xwin->gc);
        XDestroyWindow(xwin->dpy, xwin->window);
    }
    XUnlockDisplay(xwin->dpy);

    XCloseDisplay(xwin->dpy);
    pixbuf_free(&xwin->blendbuf);
    free(xwin->host);
    free(xwin->pm_buffer);
    free(xwin->r_cdtable);
    free(xwin->g_cdtable);
    free(xwin->b_cdtable);
}


/*******************************************************************************
 * Set the title string for the given X window.
 *
 * @param xwin   pointer to xwin_t instance to change title of
 * @param title  string containing new title
 *******************************************************************************/
static inline void xwin_title(xwin_t* xwin, const char* title) {
    XLockDisplay(xwin->dpy);
    XStoreName  (xwin->dpy, xwin->window, title);
    XUnlockDisplay(xwin->dpy);
}


/*******************************************************************************
 * Resize a window
 *
 * @param xwin  pointer to xwin_t instance to resize
 * @param ww    new window width
 * @param hh    new window height
 *******************************************************************************/
static inline void xwin_resize(xwin_t *xwin, uint32_t ww, uint32_t hh) {
    XResizeWindow(xwin->dpy, xwin->window, ww, hh);
}


/*******************************************************************************
 * Move a window relative to parent
 *
 * @param xwin  pointer to xwin_t instance to resize
 * @param xx    new x coordinate of top,left of window
 * @param yy    new y coordniate of top,left of window
 *******************************************************************************/
static inline void xwin_move(xwin_t *xwin, uint32_t xx, uint32_t yy) {
    XMoveWindow(xwin->dpy, xwin->window, xx, yy);
}


/*******************************************************************************
 * Takes a region from the blend buffer, converts color depth to match the window
 * and copies the pixels to the screen to make them visible.
 *
 * @param xwin  pointer to xwin_t instance to draw to
 * @param rect  rectangular region to update on screen
 *******************************************************************************/
static inline void xwin_insert_rect(xwin_t *xwin, rect_t rect) { 
    ssize_t px = (ssize_t)rect.x0;
    ssize_t py = (ssize_t)rect.y0;
    ssize_t ww = (ssize_t)rect_width (rect);
    ssize_t hh = (ssize_t)rect_height(rect);

    XLockDisplay(xwin->dpy);
    if (!xwin->pixmap || !xwin->pm_buffer || ww < 0 || hh < 0) {
        XUnlockDisplay(xwin->dpy);
        return;
    }

    pixbuf_t *pb    = &xwin->blendbuf;
    uint16_t stride = pb->stride;
    pixel_t *pixels = &pb->pixels[py*stride+px];

    // Return LSBFirst or MSBFirst depending on the endianness
    int endian      = 1;
    int endianness  = (*(char*)&endian == 1 ? LSBFirst : MSBFirst);

    // Grab color depth for the target display
    uint8_t  depth  = DefaultDepth (xwin->dpy, xwin->screen);

    // Do depth conversion using lookup tables
    int padding = 1;
    if (depth > 16) {
        padding = 32;
        uint32_t *ptr = (uint32_t*)xwin->pm_buffer;

        for (uint16_t row=0; row < hh; row++) {
            for (uint16_t col=0; col < ww; col++) {
                pixel_t pix = pixels[row*stride+col];

                // Convert depth and merge color fields together
                *ptr++ = \
                    ((uint32_t*)xwin->r_cdtable)[pix.r] |
                    ((uint32_t*)xwin->g_cdtable)[pix.g] |
                    ((uint32_t*)xwin->b_cdtable)[pix.b];
            }
        }

        // Swap byte order if needed
        if (endianness != ImageByteOrder(xwin->dpy)) {
            for (ssize_t ii=0; ii < (ww*hh); ii++) {
                ptr[ii] = \
                    (ptr[ii] & 0x000000FF) << 24 |
                    (ptr[ii] & 0x0000FF00) << 8  |
                    (ptr[ii] & 0x00FF0000) >> 8  |
                    (ptr[ii] & 0xFF000000) >> 24;
            }
        }

    // Handle 2-bytes/pixel case
    } else if (depth > 8) {
        padding       = 16;
        uint16_t *ptr = (uint16_t*)xwin->pm_buffer;

        for (uint16_t row=0; row < hh; row++) {
            for (uint16_t col=0; col < ww; col++) {
                pixel_t pix = pixels[row*stride+col];

                *ptr++ = \
                    ((uint16_t*)xwin->r_cdtable)[pix.r] |
                    ((uint16_t*)xwin->g_cdtable)[pix.g] |
                    ((uint16_t*)xwin->b_cdtable)[pix.b];
            }
        }

        if (endianness != ImageByteOrder(xwin->dpy)) {
            for (ssize_t ii=0; ii < (ww*hh); ii++) {
                ptr[ii] = \
                    (ptr[ii] & 0x00FF) << 8 |
                    (ptr[ii] & 0xFF00) >> 8;
            }
        }

    // 8-bit color, so we'll dither
    } else {
        padding = 8;
        uint8_t *ptr = (uint8_t*)xwin->pm_buffer;

        // Low image depth, dither to get a better image
        static const uint8_t clamp[8]     = {0, 1, 2, 3, 4, 5, 5, 5};
        static const float   dither[4][4] = {
            {  0/16.0,  4/16.0,  1/16.0,  5/16.0 },
            { 12/16.0,  8/16.0, 13/16.0,  9/16.0 },
            {  3/16.0,  7/16.0,  2/16.0,  6/16.0 },
            { 15/16.0, 11/16.0, 14/16.0, 10/16.0 }
        };

        const float scale = 6/255.0;
        uint8_t    *gamma = xwin->gamma;
        for (uint16_t row=0; row < hh; row++) {
            pixel_t *pixptr = pixels + row*stride;
            for (uint16_t col=0; col < ww; col++) {
                pixel_t pix = pixptr[col];
                uint8_t rr  =  clamp[(uint8_t)(scale*gamma[pix.r] + dither[col%4][row%4])];
                uint8_t gg  =  clamp[(uint8_t)(scale*gamma[pix.g] + dither[col%4][row%4])];
                uint8_t bb  =  clamp[(uint8_t)(scale*gamma[pix.b] + dither[col%4][row%4])];
                *ptr++      = xwin->palette[36*rr + 6*gg + bb];
            }
        }
    }

    // Allocate XImage structure space
    XImage *image = XCreateImage(xwin->dpy, 0, depth, ZPixmap, 0,
                                 (char*)xwin->pm_buffer, ww, hh,    padding, ww*padding/8);

    // Something went wrong, abort
    if (image == NULL) {
        XUnlockDisplay(xwin->dpy);
        return;
    }

    // Put the converted image into our X11 pixmap
    XPutImage(xwin->dpy, xwin->pixmap, xwin->gc, image, 0, 0, px, py, ww, hh);

    // And actually copy the region to the screen
    XCopyArea     (xwin->dpy, xwin->pixmap, xwin->window, xwin->gc, px, py, ww, hh, px, py);
    XFlush        (xwin->dpy);

    // Cleanup
    image->data = NULL; // detach our array from image to avoid X11 freeing our memory
    XDestroyImage(image);
    XUnlockDisplay(xwin->dpy);
}


/*******************************************************************************
 * Convenience wrapper around xwin_insert_rect that just copies the entire blendbuf
 *
 * @param xwin  pointer to xwin_t instance to update
 *******************************************************************************/
static inline void xwin_insert(xwin_t *xwin) {
    xwin_insert_rect(xwin, rectangle(0, 0, xwin->blendbuf.width, xwin->blendbuf.height));
}


/*******************************************************************************
 * Composites a pixbuffer into the blendbuffer for the window and then copies
 * dirty areas to the screen.
 *
 * @param xwin  xwin_t to update
 * @param pb    pixbuf_t to compose from
 *******************************************************************************/
static inline void xwin_composite(xwin_t *xwin, pixbuf_t *pb) {
    size_t nredraw;
    rect_t *redraw_list = pixbuf_composite(&xwin->blendbuf, pb, &nredraw);

    for (size_t ii=0; ii < nredraw; ii++) {
        rect_t dirty = redraw_list[ii];

        if (!rect_empty(dirty)) {
            xwin_insert_rect(xwin, dirty);
        }
    }
}


/********************************************************************************
 * Disable display of the X11 cursor.  Useful for displaying your own.
 *
 * @param xwin  pointer to xwin_t instance to disble cursor on
 ********************************************************************************/
static inline void xwin_no_cursor(xwin_t *xwin) {
    Cursor cursor;
    Pixmap pixmap;
    XColor black; memset(&black, 0, sizeof(XColor));
    static char data[] = {0,0,0,0,0,0,0,0};

    pixmap = XCreateBitmapFromData(xwin->dpy, xwin->window, data, 8, 8);
    cursor = XCreatePixmapCursor  (xwin->dpy, pixmap, pixmap, &black, &black, 0, 0);
    XDefineCursor(xwin->dpy, xwin->window, cursor);
    XFreeCursor  (xwin->dpy, cursor);
}



//--------------------------------------------------------------------------------
// Drawing related functions
//--------------------------------------------------------------------------------

#define MUL8(a,b) (((uint16_t)(a)*(b)) >> 8) // 8.0 x 0.8 fixed-point multiplication


/********************************************************************************
 * Put a pixel in the pixbuf. alpha blends with the pixel behind it.
 *
 * @param pb     pointer to pixbuf_t instance to receive the pixel
 * @param color  color of pixel to draw
 * @param xx     x coordinate of pixel
 * @param yx     y coordinate of pixel
 *******************************************************************************/
static inline void draw_pixel(pixbuf_t *pb, pixel_t color, uint16_t xx, uint16_t yy) {
    uint16_t stride = pb->stride;
    pixel_t  *pixel = pb->pixels + yy*stride+xx;

    if (pb->blend) {
        *pixel = blend_pixel(*pixel, color);
    } else {
        *pixel = color;
    }
}


/********************************************************************************
 * Clip a pixel and then put it in the pixbuf
 *
 * @param pb     pointer to pixbuf_t instance to receive the pixel
 * @param color  color of pixel to draw
 * @param xx     x coordinate of pixel
 * @param yx     y coordinate of pixel
 *******************************************************************************/
static inline void draw_pixel_clip(pixbuf_t *pb, pixel_t color, int16_t xx, int16_t yy) {
    if (point_in_rect(xx, yy, pb->cliprect)) {
        draw_pixel(pb, color, xx, yy);
    }
}


/********************************************************************************
 * Clip an arbitrary line to a rectangle using the Lian-Barsky algorithm.
 *
 * @param rect  rectangle we're clipping to
 * @param x0    first x coordinate of line
 * @param y0    firsy y coordinate of line
 * @param x1    second x coordinate of line
 * @param y2    second y coordinate of line
 *
 * @return false if line was removed entirely, true otherwise.
 *******************************************************************************/
static inline bool clip_line(rect_t rect,
                             float *x0, float *y0, 
                             float *x1, float *y1) {

    double dx= *x1-*x0;
    double dy= *y1-*y0;

    const double ps[4] = { -dx, dx,
                           -dy, dy };
    const double qs[4] = { *x0-rect.x0, rect.x1-*x0-1,
                           *y0-rect.y0, rect.y1-*y0-1 };

    double t0=0.0, t1=1.0;
    for (size_t kk=0; kk < 4; kk++) {
        if (ps[kk] == 0.0) {     // line is vertical/horizontal
            if (qs[kk] < 0.0) {  // and outside of the rect
                return false;    // so discard it
            }
            continue;            // otherwise continue because we can't compute ratio
        }
        
        double rr = qs[kk]/ps[kk];
        
        // check for complete clipping
        if (((ps[kk] <  0.0) && (rr > t1))  ||
            ((ps[kk] >  0.0) && (rr < t0))) {
            return false;
        }

        // find min/max of parametric parameter at intersection points
        if (ps[kk] <= 0.0) {
            t0 = fmax(rr, t0);
        }

        if (ps[kk] >  0.0) {
            t1 = fmin(rr, t1);
        }
    }

    // compute new endpoints
    *x1 = (float)(*x0 + t1*dx);
    *y1 = (float)(*y0 + t1*dy);
    *x0 = (float)(*x0 + t0*dx);
    *y0 = (float)(*y0 + t0*dy);

    return true;
}


/*******************************************************************************
 * Draw a vertical line with a bit-mask pattern applied
 *
 * @param pb     pointer to pixbuf_t instance to draw into
 * @param color  color of line
 * @param xx     x coordinate of start of line
 * @param y0     starting y coordinate of line
 * @param y1     ending y coordinate of line
 * @param tt     thickness of line in pixels
 * @param mask   bit-mask pattern to apply
 *******************************************************************************/
static inline void draw_vline_pattern(pixbuf_t *pb, pixel_t color, int16_t xx, int16_t y0, int16_t y1, uint16_t tt, uint16_t mask) {
    if (y0 > y1) { y0=y0^y1; y1=y1^y0; y0=y0^y1; } // Swap y0 and y1 if needed for loop

    uint16_t xl = pixbuf_clip_x(pb, xx),  xh = pixbuf_clip_x(pb, xx+tt);
    uint16_t yl = pixbuf_clip_y(pb, y0),  yh = pixbuf_clip_y(pb, y1+1);
    
    for (uint16_t yy=yl; yy < yh; yy++) {
        if (mask != 0xFFFF && ((mask >> (yy % 16)) & 0x1) == 0)
            continue;

        for (uint16_t xx=xl; xx < xh; xx++) {
            draw_pixel(pb, color, xx, yy);
        }
    }   
}


/********************************************************************************
 * Draw a vertical line.  Uses an efficient loop to draw pixels.
 *
 * @param pb     pointer to pixbuf_t instance to draw into
 * @param color  color of line
 * @param xx     x coordinate of start of line
 * @param y0     starting y coordinate of line
 * @param y1     ending y coordinate of line
 * @param tt     thickness of line in pixels
 *******************************************************************************/
static inline void draw_vline(pixbuf_t *pb, pixel_t color, int16_t xx, int16_t y0, int16_t y1, uint16_t tt) {
    draw_vline_pattern(pb, color, xx, y0, y1, tt, 0xFFFF);
}


/********************************************************************************
 * Draw a horizontal line with a bit-mask pattern applied
 *
 * @param pb     pointer to pixbuf_t instance to draw into
 * @param color  color of line
 * @param x1     starting x coordinate of line
 * @param x1     ending x coordinate of line
 * @param yy     y coordinate of line
 * @param tt     thickness of line in pixels
 * @param mask   bit-mask pattern to apply
 *******************************************************************************/
static inline void draw_hline_pattern(pixbuf_t *pb, pixel_t color, int16_t x0, int16_t x1, int16_t yy, uint16_t tt, uint16_t mask) {
    if (x0 > x1) { x0=x0^x1; x1=x1^x0; x0=x0^x1; } // Swap x0 and x1 if needed for loop

    uint16_t xl = pixbuf_clip_x(pb, x0),  xh = pixbuf_clip_x(pb, x1+1);
    uint16_t yl = pixbuf_clip_y(pb, yy),  yh = pixbuf_clip_y(pb, yy+tt);

    for (uint16_t xx=xl; xx < xh; xx++) {        
        if (mask != 0xFFFF && ((mask >> (xx % 16)) & 0x1) == 0)
            continue;

        for (uint16_t yy=yl; yy < yh; yy++) {
            draw_pixel(pb, color, xx, yy);
        }
    }
}


/********************************************************************************
 * Draw a horizontal line.  Uses an efficient loop to draw pixels.
 *
 * @param pb     pointer to pixbuf_t instance to draw into
 * @param color  color of line
 * @param x1     starting x coordinate of line
 * @param x1     ending x coordinate of line
 * @param yy     y coordinate of line
 * @param tt     thickness of line in pixels
 *******************************************************************************/
static inline void draw_hline(pixbuf_t *pb, pixel_t color, int16_t x0, int16_t x1, int16_t yy, uint16_t tt) {
    draw_hline_pattern(pb, color, x0, x1, yy, tt, 0xFFFF);
}


// precomputed filters.  Range is [0,1], domain is [0,255]
static const uint8_t gaus_kernel_1r[64] = {
    0x00, 0x00, 0x02, 0x04, 0x06, 0x08, 0x0B, 0x0E, 0x11, 0x15, 0x18, 0x1C, 0x20, 0x24, 0x28, 0x2D,
    0x31, 0x36, 0x3A, 0x3F, 0x44, 0x49, 0x4E, 0x53, 0x58, 0x5D, 0x62, 0x68, 0x6D, 0x72, 0x78, 0x7D,
    0x82, 0x88, 0x8D, 0x92, 0x97, 0x9D, 0xA2, 0xA7, 0xAC, 0xB1, 0xB6, 0xBB, 0xC0, 0xC5, 0xC9, 0xCE,
    0xD2, 0xD7, 0xDB, 0xDF, 0xE3, 0xE7, 0xEA, 0xEE, 0xF1, 0xF4, 0xF7, 0xF9, 0xFB, 0xFD, 0xFF, 0xFF,
};


/********************************************************************************
 * Draws antialiased line in pixbuf from (x0,y0) to (x1,y1).
 *
 * @param pb     pointer to pixbuf_t instance to draw into
 * @param color  color of line
 * @param x0     starting x coordinate of line
 * @param y0     starting y coordinate of line
 * @param x1     ending x coordinate of line
 * @param y1     ending y coordinate of line
 *******************************************************************************/
#define MINF(a,b) (((a) < (b)) ? (a) : (b))
#define MAXF(a,b) (((a) > (b)) ? (a) : (b))

static inline ssize_t fast_floor(float x) { return ((ssize_t)x - ((x < 0.0f) ? 1 : 0)); } 
static inline ssize_t fast_ceil (float x) { return ((ssize_t)x + ((x < 0.0f) ? 0 : 1)); }


typedef float __attribute__((aligned (32))) float4[4];
static inline void draw_linef(pixbuf_t *pb, pixel_t color, float x0, float y0, float x1, float y1, float tt) {
    // keep color alpha around
    const uint8_t alpha=color.a;
    uint8_t      nalpha; // new alpha

    // clip line to 8-bit subpixel precision
    if (!clip_line(rect_grow(pb->cliprect, tt+1), &x0, &y0, &x1, &y1)) {
        return;
    }
    
    // sanity check inputs
    if (tt <= 0) { tt = 1.0; }
    
    const uint8_t* kernel = gaus_kernel_1r;
    const ssize_t  radius = 1;
    const uint8_t  ksize  = 64;

    // compute components of unit normal to the line    
    float dx    = x1-x0;
    float dy    = y1-y0;
    float length = hypotf(dx, dy); if (fabs(length) <= 1e-3) return;
    dx /= length;
    dy /= length;
    
    // components of four corners of enclosing rectangle
    float hw = radius+tt/2.0; // half-width of line
    const float4 cx = {x0-hw*dy, x1-hw*dy, x1+hw*dy, x0+hw*dy};
    const float4 cy = {y0+hw*dx, y1+hw*dx, y1-hw*dx, y0-hw*dx};

    // scale normals so that 1.0 represents maximum intensity of filtering operation
    float fscale = (tt >= 2*radius) ? 1.0f/(2*radius) : 1.0/(radius+0.5*tt);
    dx *= fscale;
    dy *= fscale;

    // find max/min of coordinates to define bounding box around line
    float minx=INFINITY, maxx=-INFINITY, miny=INFINITY, maxy=-INFINITY;
    for (size_t ii=0; ii < 4; ii++) {
        if (cx[ii] < minx) minx = cx[ii];
        if (cx[ii] > maxx) maxx = cx[ii];
        if (cy[ii] < miny) miny = cy[ii];
        if (cy[ii] > maxy) maxy = cy[ii];
    }   

    // integral start/end of bounding box around the line
    ssize_t xbeg=fast_ceil(minx), xend=fast_floor(maxx);
    ssize_t ybeg=fast_ceil(miny), yend=fast_floor(maxy);
    
    // error increment vectors
    const float4 exinc = { dy, -dy, -dx, dx};
    const float4 eyinc = {-dx,  dx, -dy, dy};

    // parameters for configuring line
    float side_push = (tt/2.0f + radius) * fscale;  // amount to push out sides  
    float cap_push  = radius*fscale;                // amount to push out end cap
    
    // back out alpha multiplication so we can blend nicely.
    pixel_t raw_color = pixel_unset_alpha(color);
    
    // horizontal/vertical line
    if (dx == 0.0f || dy == 0.0f) {
        // clip to pixbuf
        ybeg = MAXF(ybeg, pb->cliprect.y0);  yend = MINF(yend, pb->cliprect.y1-1);
        xbeg = MAXF(xbeg, pb->cliprect.x0);  xend = MINF(xend, pb->cliprect.x1-1);

        // deltas to start/end of line
        float dxbeg = (x0-xbeg), dybeg = (y0-ybeg);
        float dxend = (x1-xbeg), dyend = (y1-ybeg);
        
        // starting error values
        float  dsides =  dxend*dy - dyend*dx;
        float4 error  = {\
            -dsides   + side_push,
            +dsides   + side_push,
            +dxend*dx + dyend*dy + cap_push,
            -dybeg*dy - dxbeg*dx + cap_push };

        for (ssize_t yy=ybeg; yy <= yend; yy++) {
            for (ssize_t xx=xbeg; xx <= xend; xx++) {
                float4 cerror;
                for (size_t ii=0; ii < 4; ii++) {
                    cerror[ii] = MINF(error[ii], 1.0f);
                }
                
                // determine intensity and set pixel
                uint32_t idx0 = (uint32_t)((ksize-1)*MINF(cerror[0], cerror[1]));
                //uint32_t idx1 = (uint32_t)((ksize-1)*MINF(cerror[2], cerror[3]));

                //color.a = MUL8(alpha, MUL8(kernel[idx0], kernel[idx1]));
                nalpha = MUL8(alpha, kernel[idx0]);
                draw_pixel(pb, pixel_set_alpha(raw_color, nalpha), xx, yy);

                // increment error functions               
                for (size_t ii=0; ii < 4; ii++) {
                    error[ii] += exinc[ii];
                }
            }
            
            // remove X error and add in Y error
            for (size_t ii=0; ii < 4; ii++) {
                error[ii] += eyinc[ii] - exinc[ii]*(xend-xbeg+1);
            }
        }
    } else if (fabsf(dy) >= fabsf(dx)) {
        // more vertical, loop in y direction, find lines for each side
        float m0 = (cy[1]-cy[0])/(cx[1]-cx[0]),  m1 = (cy[2]-cy[3])/(cx[2]-cx[3]);
        float b0 =  cy[1]-m0*cx[1],              b1 =  cy[2]-m1*cx[2];

        // swap if dy is negative to put lower edge on the left
        if (dy < 0.0) {
            float tmp;
            tmp = m1; m1 = m0; m0 = tmp;
            tmp = b1; b1 = b0; b0 = tmp;
        }
        
        // need reciprocal to compute x from y
        float m0inv = 1.0f/m0;
        float m1inv = 1.0f/m1;       

        // deltas to start/end of line
        float xbeg  = fast_ceil((ybeg-b0)*m0inv);
        float dxbeg = (x0-xbeg), dybeg = (y0-ybeg);
        float dxend = (x1-xbeg), dyend = (y1-ybeg);
        
        // starting error values
        float  dsides =  dxend*dy - dyend*dx;
        float4 cerror;
        float4 error = { \
            -dsides   + side_push,
            +dsides   + side_push,
            +dxend*dx + dyend*dy + cap_push,
            -dybeg*dy - dxbeg*dx + cap_push };

        // iterate over over line
        ssize_t xx=xbeg;
        pixel_t *row = pb->pixels + (ybeg*pb->stride);
        for (ssize_t yy=ybeg; yy <= yend; yy++) {
            ssize_t x0 = fast_ceil((yy-b0)*m0inv), x1 = fast_floor((yy-b1)*m1inv);
            
            // remove previous x error
            for (size_t ii=0; ii < 4; ii++) {
                error[ii] -= exinc[ii]*abs(xx-x0);
            }

            pixel_t *pixel = row+x0;
            for (xx=x0; xx <= x1; xx++, pixel++) {
                for (size_t ii=0; ii < 4; ii++) {
                    cerror[ii] = MAXF(MINF(error[ii], 1.0f), 0.0);
                }
                
                // determine intensity and set pixel
                uint32_t idx0 = (uint32_t)((ksize-1)*MINF(cerror[0], cerror[1]));
                //uint32_t idx1 = (uint32_t)((ksize-1)*MINF(cerror[2], cerror[3]));

                //color.a = MUL8(alpha, MUL8(kernel[idx0], kernel[idx1]));
                nalpha = MUL8(alpha, kernel[idx0]);

                // blend pixel into buffer
                if (point_in_rect(xx, yy, pb->cliprect)) {
                    *pixel = blend_pixel(*pixel, pixel_set_alpha(raw_color, nalpha));
                }

                // increment error functions               
                for (size_t ii=0; ii < 4; ii++) {
                    error[ii] += exinc[ii];
                }
            }

            // move to next row
            row += pb->stride;
            
            // add y error
            for (size_t ii=0; ii < 4; ii++) {
                error[ii] += eyinc[ii];
            }
        }
    } else if (fabsf(dy) < fabsf(dx)) {
        // more horizontal, loop in x direction, find lines for each side
        float m0 = (cy[1]-cy[0])/(cx[1]-cx[0]),  m1 = (cy[2]-cy[3])/(cx[2]-cx[3]);
        float b0 =  cy[1]-m0*cx[1],              b1 =  cy[2]-m1*cx[2];

        // swap if dx is positive to put lower edge on the bottom
        if (dx > 0.0) {
            float tmp;
            tmp = m1; m1 = m0; m0 = tmp;
            tmp = b1; b1 = b0; b0 = tmp;
        }

        // deltas to start/end of line
        float ybeg  = fast_ceil(m0*xbeg+b0);
        float dxbeg = (x0-xbeg), dybeg = (y0-ybeg);
        float dxend = (x1-xbeg), dyend = (y1-ybeg);
        
        // starting error values
        float  dsides =  dxend*dy - dyend*dx;
        float4 cerror;
        float4 error = { \
            -dsides   + side_push,
            +dsides   + side_push,
            +dxend*dx + dyend*dy + cap_push,
            -dybeg*dy - dxbeg*dx + cap_push };

        // iterate over over line
        ssize_t yy=ybeg;
        pixel_t *col = pb->pixels + xbeg;
        for (ssize_t xx=xbeg; xx <= xend; xx++, col++) {
            ssize_t y0 = fast_ceil(m0*xx+b0), y1=fast_floor(m1*xx+b1);

            // remove previous y error
            for (size_t ii=0; ii < 4; ii++) {
                error[ii] -= eyinc[ii]*abs(yy-y0);
            }

            pixel_t *pixel = col + y0*pb->stride;
            for (yy=y0; yy <= y1; yy++, pixel += pb->stride) {
                // clamp error to [0,1], pixels out of bounds will get zero intensity blended
                for (size_t ii=0; ii < 4; ii++) {
                    cerror[ii] = MAXF(MINF(error[ii], 1.0f), 0.0f);
                }
                
                // determine intensity and set pixel
                uint32_t idx0 = (uint32_t)((ksize-1)*MINF(cerror[0], cerror[1]));
                //uint32_t idx1 = (uint32_t)((ksize-1)*MINF(cerror[2], cerror[3]));

                //color.a = MUL8(alpha, MUL8(kernel[idx0], kernel[idx1]));
                nalpha = MUL8(alpha, kernel[idx0]);
                if (point_in_rect(xx, yy, pb->cliprect)) {
                    *pixel = blend_pixel(*pixel, pixel_set_alpha(raw_color, nalpha));
                }
                
                // increment error functions               
                for (size_t ii=0; ii < 4; ii++) {
                    error[ii] += eyinc[ii];
                }
            }

            // add y error
            for (size_t ii=0; ii < 4; ii++) {
                error[ii] += exinc[ii];
            }
        }
    }
}


/********************************************************************************
 * Draws antialiased line in pixbuf from (x0,y0) to (x1,y1).
 *
 * @param pb     pointer to pixbuf_t instance to draw into
 * @param color  color of line
 * @param x0     starting x coordinate of line
 * @param y0     starting y coordinate of line
 * @param x1     ending x coordinate of line
 * @param y1     ending y coordinate of line
 *******************************************************************************/
static inline void draw_line(pixbuf_t *pb, pixel_t color, int64_t x0, int64_t y0, int64_t x1, int64_t y1) {
    // Keep color alpha around
    const uint8_t alpha=color.a;
    uint8_t      nalpha; // new alpha
    
    // Clip the line
    float fx0 = x0, fy0 = y0;
    float fx1 = x1, fy1 = y1;
    if (!clip_line(pb->cliprect, &fx0, &fy0, &fx1, &fy1)) {
        return;
    }
    x0 = (int64_t)fx0;  y0 = (int64_t)fy0;  
    x1 = (int64_t)fx1;  y1 = (int64_t)fy1;  
    
    
    // Make line run top to bottom, XOR swap coordinates if needed
    if (y0 > y1) {
        x0=x0^x1; x1=x1^x0; x0=x0^x1;
        y0=y0^y1; y1=y1^y0; y0=y0^y1;
    }

    // Draw first pixel
    draw_pixel(pb, color, x0, y0);

    // Determine x direction
    int16_t dx   = x1-x0;
    int16_t dy   = y1-y0;
    int16_t xdir = 1;
    if (dx < 0) {
        xdir = -1;
        dx   = -dx;
    }

    // Special case horizontal
    if (dy == 0) {
        draw_hline(pb, color, x0, x1, y0, 1);
        return;
    }

    // Special case vertical
    if (dx == 0) {
        draw_vline(pb, color, x0, y0, y1, 1);
        return;
    }

    // Special case diagonal
    if (dx == dy) {
        do {
            x0 += xdir;
            y0 += 1;
            draw_pixel(pb, color, x0, y0);
        } while (--dy != 0);
        return;
    }

    const uint16_t levels = 256;
    const uint16_t bits   = 8;
    const uint16_t ishift = 16 - bits; // How much to shift to get bits MSBs
    const uint16_t wmask  = levels-1;  // Weighting mask for complement
    uint16_t       error  = 0, error_tmp, error_adj, weighting;

    // backout alpha pre-multiplication for blending
    pixel_t raw_color = pixel_unset_alpha(color);
    
    // We'll use fixed-point to implement Wu's algorithm for speed's sake
    if (dy > dx) {
        // Line is more vertical than horizontal
        error_adj = ((uint32_t)dx << 16) / (uint32_t)dy;

        while (--dy > 0) {
            error_tmp  = error;
            error     += error_adj; // Calculate error for this pixel
            if (error <= error_tmp) {
                // error wrapped around, advance x coordinate
                x0 += xdir;
            }
            y0++;

            // Get intensity for this pixel and paired pixel intensity complement
            // Multiply the current alpha by the weighting to get the actual alpha
            weighting = error >> ishift;
            nalpha = MUL8(alpha, weighting ^ wmask); draw_pixel(pb, pixel_set_alpha(raw_color, nalpha), x0,        y0);
            nalpha = MUL8(alpha, weighting);         draw_pixel(pb, pixel_set_alpha(raw_color, nalpha), x0 + xdir, y0);
        }

        nalpha = alpha; draw_pixel(pb, pixel_set_alpha(raw_color, nalpha), x1, y1);
    } else {
        // More horizontal than vertical
        error_adj = ((uint32_t)dy << 16) / (uint32_t)dx;

        while (--dx > 0) {
            error_tmp  = error;
            error     += error_adj;
            if (error <= error_tmp) {
                y0++;
            }
            x0 += xdir;

            // Get intensity for this pixel and paired pixel intensity complement
            // Multiply the current alpha by the weighting to get the actual alpha
            weighting = error >> ishift;
            nalpha = MUL8(alpha, weighting ^ wmask); draw_pixel(pb, pixel_set_alpha(raw_color, nalpha), x0, y0);
            nalpha = MUL8(alpha, weighting);         draw_pixel(pb, pixel_set_alpha(raw_color, nalpha), x0, y0+1);
        }

        nalpha = alpha; draw_pixel(pb, pixel_set_alpha(raw_color, nalpha), x1, y1);
    }
}


/*******************************************************************************
 * Draws an open rectangle into a pixbuf.
 *
 * @param pb    pixbuf_t instance to draw into
 * @param color color of rectangle
 * @param x0    x coordinate of top,left corner of rectangle
 * @param y0    y coordinate of top,left corner of rectangle
 * @param ww    width of rectangle
 * @param hh    height of rectangle
 * @param tt    thickness of rectangle in pixels
 *******************************************************************************/
static inline void draw_rect(pixbuf_t *pb, pixel_t color, rect_t rect, uint16_t tt) { 
    int32_t x0=rect.x0,           y0=rect.y0;
    int32_t ww=rect_width(rect), hh=rect_height(rect);

    // Clamp thickness to avoid duplicate drawing
    // Add one if the minimum dimension is odd and we clamp to it, this will cause us to draw a center pixel
    uint16_t min_dim = (ww > hh) ? hh : ww;
    tt = (tt > min_dim/2) ? min_dim/2 + (min_dim & 0x1): tt;

    for (size_t ii=0; ii < tt; ii++) {
        uint16_t xl=x0, xh=x0+ww-1;
        uint16_t yl=y0, yh=y0+hh-1;

        if (ww >= 2) {
            draw_hline(pb, color, xl, xh, yl, 1);
            draw_hline(pb, color, xh, xl, yh, 1);
        }

        if (hh >  2) {
            // Note we adjust the y values so horizontal/vertical lines do _not_ overlap, this it to avoid alpha blending problems
            draw_vline(pb, color, xh, yl+1, yh-1, 1);
            draw_vline(pb, color, xl, yl+1, yh-1, 1);
        }

        if (hh == 1 && ww == 1) {
            if (point_in_rect(xl,yl,pb->cliprect)) {
                draw_pixel(pb, color, xl, yl);
            }
        }

        // Move inward
        x0++;
        y0++;
        ww -= 2;
        hh -= 2;
    }
}


/*******************************************************************************
 * Draws an open rectangle into a pixbuf with a pattern
 *
 * @param pb    pixbuf_t instance to draw into
 * @param color color of rectangle
 * @param x0    x coordinate of top,left corner of rectangle
 * @param y0    y coordinate of top,left corner of rectangle
 * @param ww    width of rectangle
 * @param hh    height of rectangle
 * @param tt    thickness of rectangle in pixels
 * @param mask  pattern mask to apply to rectangle
 *******************************************************************************/
static inline void draw_rect_pattern(pixbuf_t *pb, pixel_t color, rect_t rect, uint16_t tt, uint16_t mask) { 
    int32_t x0=rect.x0,           y0=rect.y0;
    int32_t ww=rect_width(rect), hh=rect_height(rect);

    // Clamp thickness to avoid duplicate drawing
    // Add one if the minimum dimension is odd and we clamp to it, this will cause us to draw a center pixel
    uint16_t min_dim = (ww > hh) ? hh : ww;
    tt = (tt > min_dim/2) ? min_dim/2 + (min_dim & 0x1): tt;

    for (size_t ii=0; ii < tt; ii++) {
        uint16_t xl=x0, xh=x0+ww-1;
        uint16_t yl=y0, yh=y0+hh-1;

        if (ww >= 2) {
            draw_hline_pattern(pb, color, xl, xh, yl, 1, mask);
            draw_hline_pattern(pb, color, xh, xl, yh, 1, mask);
        }

        if (hh >  2) {
            // Note we adjust the y values so horizontal/vertical lines do _not_ overlap, this it to avoid alpha blending problems
            draw_vline_pattern(pb, color, xh, yl+1, yh-1, 1, mask);
            draw_vline_pattern(pb, color, xl, yl+1, yh-1, 1, mask);
        }

        if (hh == 1 && ww == 1) {
            if (point_in_rect(xl,yl,pb->cliprect)) {
                draw_pixel(pb, color, xl, yl);
            }
        }

        // Move inward
        x0++;
        y0++;
        ww -= 2;
        hh -= 2;
    }
}


/*******************************************************************************
 * Draws a filled rectangle into a pixbuf
 *
 * @param pb     pixbuf_t instance to draw into
 * @param color  color of rectangle
 * @param x0     x coordinate of top,left corner of rectangle
 * @param y0     y coordinate of top,left corner of rectangle
 * @param ww     width of rectangle
 * @param hh     height of rectangle
 *******************************************************************************/
static inline void draw_rect_fill(pixbuf_t *pb, pixel_t color, rect_t rect) {
    uint16_t xl=pixbuf_clip_x(pb, rect.x0),  xh=pixbuf_clip_x(pb, rect.x1);
    uint16_t yl=pixbuf_clip_y(pb, rect.y0),  yh=pixbuf_clip_y(pb, rect.y1);

    for (uint16_t yy=yl; yy < yh; yy++) {
        for (uint16_t xx=xl; xx < xh; xx++) {
            draw_pixel(pb, color, xx, yy);
        }
    }
}


// Constants for circle quadrants
#define IM_QUADRANT_1 1
#define IM_QUADRANT_2 2
#define IM_QUADRANT_3 4
#define IM_QUADRANT_4 8
#define IM_TOP_HALF    (IM_QUADRANT_1 | IM_QUADRANT_2)
#define IM_BOTTOM_HALF (IM_QUADRANT_3 | IM_QUADRANT_4)
#define IM_LEFT_HALF   (IM_QUADRANT_2 | IM_QUADRANT_3)
#define IM_RIGHT_HALF  (IM_QUADRANT_1 | IM_QUADRANT_4)
#define IM_FULL_CIRCLE (IM_TOP_HALF   | IM_BOTTOM_HALF)


/********************************************************************************
 * Internal function forming the core of the circle drawing.
 *
 * @param pb     pointer to pixbuf_t instance to draw into
 * @param color  color of circle
 * @param x0     x coordinate of top,left corner of square box around circle
 * @param y0     y coordinate of top,left corner of square box around circle
 * @param rr     radius of circle, in pixels
 * @param fill   0 for empty antialiased circle, 1 for filled
 *******************************************************************************/
static inline void draw_circ_intern(pixbuf_t *pb, pixel_t  color,
                                    int16_t   x0, int16_t  y0, uint16_t rr,  int fill, uint8_t quadrants) {

    uint8_t nalpha; // new alpha
    
    // Zero radius circle's easy enough
    if (rr == 0) {
        return;
    }

    // XXX: Check for circle entirely within clip region and use regular draw_pixel if it is.
    
    // Adjust x0/y0 to compensate for radius
    x0 += rr;
    y0 += rr;

#define QUADRANT1 ((quadrants) & IM_QUADRANT_1)
#define QUADRANT2 ((quadrants) & IM_QUADRANT_2)
#define QUADRANT3 ((quadrants) & IM_QUADRANT_3)
#define QUADRANT4 ((quadrants) & IM_QUADRANT_4)

    // backout pre-multiplication for blending
    pixel_t raw_color = pixel_unset_alpha(color);

#define DRAW4(x,y)                                          \
    if (QUADRANT4) draw_pixel_clip(pb, pixel_set_alpha(raw_color, nalpha),  (x)+x0,  (y)+y0); \
    if (QUADRANT1) draw_pixel_clip(pb, pixel_set_alpha(raw_color, nalpha),  (x)+x0, -(y)+y0); \
    if (QUADRANT3) draw_pixel_clip(pb, pixel_set_alpha(raw_color, nalpha), -(x)+x0,  (y)+y0); \
    if (QUADRANT2) draw_pixel_clip(pb, pixel_set_alpha(raw_color, nalpha), -(x)+x0, -(y)+y0); \


    uint8_t  alpha=color.a;
    uint16_t xx,yy,tt;

    // Draw four "cardinal" pixels
    if (QUADRANT1 || QUADRANT4) draw_pixel_clip(pb, color,  rr+x0,     y0);
    if (QUADRANT2 || QUADRANT3) draw_pixel_clip(pb, color, -rr+x0,     y0);
    if (QUADRANT3 || QUADRANT4) draw_pixel_clip(pb, color,     x0,  rr+y0);
    if (QUADRANT1 || QUADRANT2) draw_pixel_clip(pb, color,     x0, -rr+y0);

    // Value of xx and yy at 45 degrees
    uint16_t rr_sq2 = lrint((double)rr/M_SQRT2);

    // Draw y-dominant portion
    xx=rr, yy=0, tt=0;
    while (xx > yy) {
        yy++;

        // The D(r,j) function can be pre-computed to speed things up
        float dist  = sqrtf((rr+yy)*(rr-yy));
        uint8_t Drj = lrintf(floorf(255 * (ceilf(dist) - dist) + .5));

        // Condition for moving the x coordinate over one point
        if (tt > Drj) {
            xx--;
        }

        // Break condition, have to take special care at 45 degrees
        if (((xx == yy) && (xx != rr_sq2)) | (xx < yy)) {
            break;
        }

        // Draw border pixels, only draw inner pixel if we're not filling
        nalpha = MUL8(alpha, 255^Drj); DRAW4(xx,   yy);
        if (!fill) {
            nalpha = MUL8(alpha, Drj); DRAW4(xx-1, yy);
        }

        // Fill interior of circle if requested
        if (fill) {
            for (int16_t xf=xx-1; xf > 0; xf--) {
                nalpha = alpha; DRAW4(xf, yy);
            }
        }

        tt=Drj;
    }


    // Now draw x-dominant portions
    xx=0; yy=rr; tt=0;
    while (yy > xx) {
        xx++;

        // The D(r,j) function can be pre-computed to speed things up
        float dist  = sqrtf((rr+xx)*(rr-xx));
        uint8_t Drj = lrintf(floorf(255 * (ceilf(dist) - dist) + .5));

        // Condition for moving the y coordinate down one point
        bool ychange = (tt > Drj);
        if (ychange) {
            yy--;
        }

        // Quit at 45 degrees, loop above handles edge case
        if (yy <= xx) {
            break;
        }

        // Draw border pixels, only draw inner pixel if were not filling
        nalpha = MUL8(alpha, 255^Drj); DRAW4(xx,yy);
        if (!fill) {
            nalpha = MUL8(alpha, Drj); DRAW4(xx,yy-1);
        }

        // Fill interior of circle if we're at a new y value
        // Only do this on new y value to avoid double painting lines
        if (ychange && fill) {
            for (int16_t xf=xx-1; xf > 0; xf--) {
                nalpha = alpha; DRAW4(xf, yy);
            }
        }

        tt=Drj;
    }

    // Fill center "cross" of circle where Y=0 or X=0
    if (fill) {
        pixel_t color = pixel_set_alpha(raw_color, alpha);
        draw_pixel_clip(pb, color,  x0, y0);
        for (size_t ii=1; ii < rr; ii++) {
            if (QUADRANT1 | QUADRANT4) draw_pixel_clip(pb, color,  ii+x0,     y0);
            if (QUADRANT2 | QUADRANT3) draw_pixel_clip(pb, color, -ii+x0,     y0);
            if (QUADRANT3 | QUADRANT4) draw_pixel_clip(pb, color,     x0,  ii+y0);
            if (QUADRANT1 | QUADRANT2) draw_pixel_clip(pb, color,     x0, -ii+y0);
        }
    }

#undef DRAW4
#undef QUADRANT1
#undef QUADRANT2
#undef QUADRANT3
#undef QUADRANT4
}


/********************************************************************************
 * Draw outlined circle to pixbuf
 *
 * @param pb         pointer to pixbuf_t instance to draw into
 * @param color      color of circle
 * @param x0         x coordinate of top,left corner of square box around circle
 * @param y0         y coordinate of top,left corner of square box around circle
 * @param rr         radius of circle, in pixels
 * @param quadrants  OR-ed mask of quadrants to draw
 *******************************************************************************/
static inline void draw_circ(pixbuf_t *pb, pixel_t color,
                             int16_t   x0, int16_t y0, uint16_t rr, uint8_t quadrants) {
    draw_circ_intern(pb, color, x0, y0, rr, 0, quadrants);
}


/********************************************************************************
 * Draw filled circle to pixbuf
 *
 * @param pb         pointer to pixbuf_t instance to draw into
 * @param color      color of circle
 * @param x0         x coordinate of top,left corner of square box around circle
 * @param y0         y coordinate of top,left corner of square box around circle
 * @param rr         radius of circle, in pixels
 * @param quadrants  OR-ed mask of quadrants to draw
 *******************************************************************************/
static inline void draw_circ_fill(pixbuf_t *pb, pixel_t color,
                                  int16_t   x0, int16_t y0, uint16_t rr, uint8_t quadrants) {
    draw_circ_intern(pb, color, x0, y0, rr, 1, quadrants);
}


//--------------------------------------------------------------------------------
// Font related functions
//--------------------------------------------------------------------------------


// Define default font, rendered from Iconsolata font at 14pt
static const uint8_t /* '!' */ font_default_033[30] = \
  {0x06,0x2C,0x00,0x57,0xF3,0x00,0x54,0xF2,0x00,0x40,0xDF,0x00,0x33,0xCD,0x00,0x26,0xBC,0x00,0x19,0xAB,
   0x00,0x04,0x1C,0x00,0x2C,0x7F,0x01,0x58,0xD7,0x06};

static const uint8_t /* '"' */ font_default_034[25] = \
  {0x00,0x2C,0x00,0x22,0x0A,0x17,0xFB,0x06,0xBB,0x5D,0x18,0xD4,0x00,0xB8,0x32,0x4E,0x8C,0x04,0xD5,0x03,
   0x11,0x14,0x03,0x23,0x00};

static const uint8_t /* '#' */ font_default_035[63] = \
  {0x00,0x00,0x79,0x1D,0x3C,0x59,0x00,0x00,0x00,0xC6,0x0F,0x71,0x66,0x00,0x29,0x5E,0xE5,0x64,0xB7,0x93,
   0x32,0x26,0x53,0xDB,0x47,0xC8,0x5C,0x1E,0x00,0x1F,0xB1,0x00,0xCC,0x0B,0x00,0x31,0x77,0xBB,0x5A,0xE5,
   0x63,0x1A,0x38,0x93,0x9E,0x53,0xD8,0x43,0x0E,0x00,0x78,0x57,0x25,0xAE,0x00,0x00,0x00,0x79,0x30,0x35,
   0x78,0x00,0x00};

static const uint8_t /* '$' */ font_default_036[77] = \
  {0x00,0x00,0x00,0x14,0x03,0x00,0x00,0x00,0x00,0x13,0xD9,0x28,0x00,0x00,0x00,0x56,0xCF,0xEB,0xBB,0xA4,
   0x01,0x00,0xDA,0x17,0xCC,0x04,0x37,0x00,0x00,0xB1,0x85,0xD2,0x04,0x00,0x00,0x00,0x14,0x9F,0xFA,0x94,
   0x1A,0x00,0x00,0x00,0x00,0xCC,0x67,0xE0,0x11,0x00,0x04,0x00,0xCC,0x04,0xAD,0x3F,0x0A,0xC1,0x3D,0xCF,
   0x4E,0xDF,0x10,0x00,0x38,0xA2,0xF5,0xA4,0x2A,0x00,0x00,0x00,0x00,0x86,0x03,0x00,0x00};

static const uint8_t /* '%' */ font_default_037[63] = \
  {0x1A,0xB5,0xA6,0x08,0x01,0x96,0x13,0x86,0x56,0x91,0x5C,0x50,0x9E,0x00,0x82,0x64,0x92,0x5C,0xCD,0x1C,
   0x00,0x14,0xAB,0x99,0x6B,0x85,0x00,0x00,0x00,0x00,0x0B,0xD0,0x0E,0x00,0x00,0x00,0x00,0x7C,0x6B,0x49,
   0x5C,0x02,0x00,0x16,0xCC,0x42,0xB6,0x99,0x69,0x00,0x93,0x51,0x68,0x70,0x49,0x8E,0x22,0xBC,0x01,0x17,
   0xB2,0xAE,0x29};

static const uint8_t /* '&' */ font_default_038[70] = \
  {0x00,0x11,0x8D,0x9F,0x27,0x00,0x00,0x00,0x99,0x8A,0x56,0xD0,0x00,0x00,0x00,0xA3,0x4D,0x0F,0xD3,0x00,
   0x00,0x00,0x3F,0xCF,0xA7,0x64,0x00,0x00,0x00,0x34,0xEC,0xB4,0x00,0x01,0x00,0x17,0xDF,0x29,0xC9,0x4D,
   0x6E,0x4C,0x63,0x9D,0x00,0x21,0xDC,0xD6,0x14,0x5D,0xD0,0x0F,0x21,0xD1,0xCC,0x06,0x05,0xA0,0xE6,0xC2,
   0x37,0x9C,0x3C,0x00,0x00,0x00,0x00,0x00,0x01,0x00};

static const uint8_t /* ''' */ font_default_039[15] = \
  {0x00,0x25,0x07,0x00,0xC8,0x4D,0x00,0xCC,0x21,0x0A,0xCE,0x00,0x05,0x20,0x00};

static const uint8_t /* '(' */ font_default_040[65] = \
  {0x00,0x00,0x00,0x02,0x1B,0x00,0x00,0x19,0xC2,0x75,0x00,0x08,0xC8,0x5B,0x00,0x00,0x6A,0x9D,0x00,0x00,
   0x00,0xD1,0x2E,0x00,0x00,0x04,0xF0,0x01,0x00,0x00,0x14,0xE5,0x00,0x00,0x00,0x01,0xF4,0x05,0x00,0x00,
   0x00,0xC3,0x47,0x00,0x00,0x00,0x5D,0xBB,0x01,0x00,0x00,0x03,0xBD,0x75,0x00,0x00,0x00,0x16,0xC6,0x80,
   0x00,0x00,0x00,0x05,0x2A};

static const uint8_t /* ')' */ font_default_041[65] = \
  {0x00,0x17,0x00,0x00,0x00,0x00,0xC0,0x85,0x02,0x00,0x00,0x0C,0xB5,0x85,0x00,0x00,0x00,0x0E,0xDD,0x32,
   0x00,0x00,0x00,0x6C,0xA2,0x00,0x00,0x00,0x22,0xDA,0x00,0x00,0x00,0x06,0xF7,0x00,0x00,0x00,0x1F,0xDA,
   0x00,0x00,0x00,0x62,0xA4,0x00,0x00,0x08,0xD5,0x35,0x00,0x04,0xA1,0x90,0x00,0x01,0xBF,0x9A,0x04,0x00,
   0x00,0x30,0x00,0x00,0x00};

static const uint8_t /* '*' */ font_default_042[49] = \
  {0x00,0x00,0x05,0x40,0x01,0x00,0x00,0x00,0x00,0x06,0xEA,0x00,0x00,0x00,0x35,0x90,0x1C,0xBE,0x27,0x98,
   0x13,0x10,0x5D,0xAC,0xF0,0x9C,0x50,0x09,0x00,0x02,0xA7,0x95,0x82,0x00,0x00,0x00,0x82,0x90,0x01,0xBA,
   0x52,0x00,0x00,0x37,0x0B,0x00,0x20,0x1E,0x00};

static const uint8_t /* '+' */ font_default_043[49] = \
  {0x00,0x00,0x00,0x45,0x00,0x00,0x00,0x00,0x00,0x00,0xDC,0x00,0x00,0x00,0x00,0x00,0x00,0xDC,0x00,0x00,
   0x00,0x56,0xDC,0xDC,0xFB,0xDC,0xDC,0x37,0x00,0x00,0x00,0xDC,0x00,0x00,0x00,0x00,0x00,0x00,0xDC,0x00,
   0x00,0x00,0x00,0x00,0x00,0x7C,0x00,0x00,0x00};

static const uint8_t /* ',' */ font_default_044[15] = \
  {0x18,0x44,0x00,0x72,0xFF,0x17,0x06,0xE4,0x05,0x65,0x65,0x00,0x21,0x01,0x00};

static const uint8_t /* '-' */ font_default_045[12] = \
  {0x0B,0xDC,0xDC,0xDC,0xDC,0xC8,0x01,0x10,0x10,0x10,0x10,0x0F};

static const uint8_t /* '.' */ font_default_046[6] = \
  {0x2D,0x7A,0x01,0x5C,0xD6,0x04};

static const uint8_t /* '/' */ font_default_047[66] = \
  {0x00,0x00,0x00,0x00,0x00,0x1B,0x00,0x00,0x00,0x00,0x32,0xCC,0x00,0x00,0x00,0x00,0xA6,0x5C,0x00,0x00,
   0x00,0x1F,0xDE,0x05,0x00,0x00,0x00,0x8F,0x73,0x00,0x00,0x00,0x10,0xE4,0x0E,0x00,0x00,0x00,0x77,0x8A,
   0x00,0x00,0x00,0x07,0xE0,0x1C,0x00,0x00,0x00,0x60,0xA1,0x00,0x00,0x00,0x01,0xD3,0x2E,0x00,0x00,0x00,
   0x07,0x61,0x00,0x00,0x00,0x00};

static const uint8_t /* '0' */ font_default_048[63] = \
  {0x00,0x00,0x5C,0xA1,0x4A,0x00,0x00,0x00,0x68,0xA8,0x39,0xC4,0x48,0x00,0x06,0xD6,0x0B,0x00,0x79,0xC4,
   0x00,0x2B,0xB2,0x00,0x3A,0xC3,0xDF,0x04,0x48,0x9B,0x17,0xCE,0x1C,0xC6,0x1E,0x35,0xB5,0xBB,0x3F,0x00,
   0xD0,0x0F,0x10,0xF8,0x71,0x00,0x0B,0xDA,0x00,0x00,0x9A,0x85,0x03,0x86,0x7D,0x00,0x00,0x0B,0xA7,0xE2,
   0x98,0x06,0x00};

static const uint8_t /* '1' */ font_default_049[36] = \
  {0x00,0x36,0xA7,0x14,0x7D,0xB4,0xF0,0x1C,0x01,0x00,0xDC,0x1C,0x00,0x00,0xDC,0x1C,0x00,0x00,0xDC,0x1C,
   0x00,0x00,0xDC,0x1C,0x00,0x00,0xDC,0x1C,0x00,0x00,0xDC,0x1C,0x00,0x00,0xD9,0x1C};

static const uint8_t /* '2' */ font_default_050[54] = \
  {0x08,0x71,0xA6,0x6C,0x04,0x00,0xA1,0x77,0x38,0xB0,0x88,0x00,0x0F,0x00,0x00,0x18,0xDD,0x00,0x00,0x00,
   0x00,0x34,0xC3,0x00,0x00,0x00,0x10,0xC9,0x43,0x00,0x00,0x0D,0xC6,0x62,0x00,0x00,0x04,0xBE,0x62,0x00,
   0x00,0x00,0x72,0x94,0x00,0x00,0x00,0x00,0xF1,0xDE,0xD8,0xD8,0xDD,0x04};

static const uint8_t /* '3' */ font_default_051[54] = \
  {0x00,0x16,0x88,0xA6,0x57,0x00,0x00,0x6B,0x5A,0x42,0xCC,0x52,0x00,0x00,0x00,0x00,0x60,0x8F,0x00,0x00,
   0x03,0x25,0xBF,0x3F,0x00,0x00,0x6D,0xDF,0xB1,0x06,0x00,0x00,0x00,0x01,0x7A,0x82,0x00,0x00,0x00,0x00,
   0x2A,0xC1,0x01,0x7A,0x18,0x03,0x94,0x8D,0x01,0x6A,0xD8,0xDE,0xA1,0x0D};

static const uint8_t /* '4' */ font_default_052[63] = \
  {0x00,0x00,0x00,0x06,0xA3,0x11,0x00,0x00,0x00,0x00,0x7E,0xFE,0x18,0x00,0x00,0x00,0x2C,0xAA,0xE5,0x18,
   0x00,0x00,0x04,0xBB,0x1B,0xE4,0x18,0x00,0x00,0x75,0x67,0x00,0xE4,0x18,0x00,0x24,0xD9,0x46,0x44,0xEC,
   0x56,0x0E,0x33,0x94,0x94,0x94,0xF5,0xA2,0x1F,0x00,0x00,0x00,0x00,0xE4,0x18,0x00,0x00,0x00,0x00,0x00,
   0xE1,0x18,0x00};

static const uint8_t /* '5' */ font_default_053[63] = \
  {0x00,0x68,0xB4,0xB4,0xB4,0x90,0x00,0x00,0xA2,0x5A,0x28,0x28,0x20,0x00,0x00,0xB3,0x2F,0x00,0x00,0x00,
   0x00,0x00,0xC5,0x8E,0xA6,0x77,0x08,0x00,0x00,0xA2,0x5C,0x23,0x99,0xA4,0x00,0x00,0x00,0x00,0x00,0x07,
   0xF3,0x06,0x00,0x00,0x00,0x00,0x01,0xF0,0x0D,0x05,0xAA,0x2C,0x00,0x64,0xC7,0x00,0x00,0x42,0xC5,0xDB,
   0xB9,0x22,0x00};

static const uint8_t /* '6' */ font_default_054[63] = \
  {0x00,0x00,0x3A,0xA2,0x97,0x2A,0x00,0x00,0x3C,0xD2,0x41,0x4C,0x61,0x00,0x00,0xB9,0x3F,0x00,0x00,0x00,
   0x00,0x02,0xEF,0x38,0x8A,0x61,0x02,0x00,0x0E,0xF7,0xB0,0x50,0xAA,0x86,0x00,0x0B,0xF3,0x08,0x00,0x0D,
   0xE5,0x00,0x01,0xE9,0x0C,0x00,0x01,0xE9,0x01,0x00,0x93,0x85,0x01,0x53,0xB3,0x00,0x00,0x0A,0xA3,0xDB,
   0xBA,0x1D,0x00};

static const uint8_t /* '7' */ font_default_055[45] = \
  {0xA9,0xB4,0xB4,0xB4,0xA2,0x39,0x3C,0x3C,0x8F,0xA6,0x00,0x00,0x00,0xC7,0x41,0x00,0x00,0x2E,0xDB,0x01,
   0x00,0x00,0x92,0x77,0x00,0x00,0x04,0xE7,0x1C,0x00,0x00,0x46,0xC4,0x00,0x00,0x00,0xA0,0x6F,0x00,0x00,
   0x08,0xEC,0x1C,0x00,0x00};

static const uint8_t /* '8' */ font_default_056[63] = \
  {0x00,0x05,0x6F,0xA8,0x75,0x06,0x00,0x00,0x82,0x9E,0x2E,0xAC,0x84,0x00,0x00,0xB5,0x3D,0x00,0x47,0xAD,
   0x00,0x00,0x57,0xC1,0x22,0xB4,0x49,0x00,0x00,0x09,0xC5,0xE8,0xB9,0x06,0x00,0x00,0xB5,0x59,0x08,0x8E,
   0x9D,0x00,0x16,0xE3,0x01,0x00,0x03,0xEC,0x02,0x08,0xEA,0x3B,0x03,0x59,0xD0,0x00,0x00,0x3D,0xCB,0xE2,
   0xBD,0x27,0x00};

static const uint8_t /* '9' */ font_default_057[54] = \
  {0x00,0x05,0x76,0xA0,0x4E,0x00,0x00,0x96,0x8D,0x37,0xC1,0x57,0x02,0xE6,0x04,0x00,0x2E,0xBC,0x05,0xE8,
   0x01,0x00,0x08,0xDE,0x00,0xB0,0x76,0x23,0x98,0xF9,0x00,0x11,0x92,0xB3,0x59,0xEB,0x00,0x00,0x00,0x00,
   0x30,0xC5,0x00,0x36,0x09,0x0E,0xC2,0x5B,0x00,0x73,0xDD,0xE1,0x72,0x01};

static const uint8_t /* ':' */ font_default_058[21] = \
  {0x00,0x06,0x00,0x68,0xEC,0x04,0x1F,0x5C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x2D,0x7A,0x01,0x5C,0xD6,
   0x04};

static const uint8_t /* ';' */ font_default_059[27] = \
  {0x52,0xC2,0x04,0x36,0x8B,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x18,0x44,0x00,0x72,0xFF,0x17,0x06,0xE4,
   0x05,0x65,0x65,0x00,0x21,0x01,0x00};

static const uint8_t /* '<' */ font_default_060[56] = \
  {0x00,0x00,0x00,0x00,0x0D,0x80,0x3C,0x00,0x00,0x06,0x6E,0xD8,0x78,0x09,0x02,0x5D,0xD1,0x81,0x0E,0x00,
   0x00,0x7B,0xDF,0x1B,0x00,0x00,0x00,0x00,0x06,0x7A,0xD3,0x52,0x00,0x00,0x00,0x00,0x00,0x1D,0xAB,0xBE,
   0x2E,0x00,0x00,0x00,0x00,0x00,0x43,0xD3,0x3F,0x00,0x00,0x00,0x00,0x00,0x05,0x14};

static const uint8_t /* '=' */ font_default_061[35] = \
  {0x0D,0x20,0x20,0x20,0x20,0x20,0x08,0x48,0xB8,0xB8,0xB8,0xB8,0xB8,0x2E,0x00,0x00,0x00,0x00,0x00,0x00,
   0x00,0x18,0x3C,0x3C,0x3C,0x3C,0x3C,0x0F,0x3F,0xA0,0xA0,0xA0,0xA0,0xA0,0x28};

static const uint8_t /* '>' */ font_default_062[56] = \
  {0x58,0x6E,0x07,0x00,0x00,0x00,0x00,0x13,0x8B,0xD5,0x5C,0x02,0x00,0x00,0x00,0x00,0x1A,0x95,0xCB,0x4A,
   0x01,0x00,0x00,0x00,0x00,0x30,0xF0,0x58,0x00,0x00,0x04,0x69,0xD7,0x64,0x02,0x00,0x43,0xD0,0x94,0x11,
   0x00,0x00,0x65,0xC4,0x30,0x00,0x00,0x00,0x00,0x19,0x01,0x00,0x00,0x00,0x00,0x00};

static const uint8_t /* '?' */ font_default_063[60] = \
  {0x00,0x00,0x0E,0x3E,0x17,0x00,0x00,0x72,0xE2,0xB5,0xEF,0x4F,0x05,0x8B,0x0E,0x00,0x5A,0xD5,0x00,0x00,
   0x00,0x00,0x4B,0xE2,0x00,0x00,0x00,0x0C,0xCC,0x6E,0x00,0x00,0x00,0x97,0x7E,0x00,0x00,0x00,0x00,0xE2,
   0x09,0x00,0x00,0x00,0x00,0x42,0x00,0x00,0x00,0x00,0x08,0x7A,0x15,0x00,0x00,0x00,0x1D,0xE3,0x3A,0x00
   };

static const uint8_t /* '@' */ font_default_064[63] = \
  {0x00,0x01,0x5E,0xA4,0x7A,0x13,0x00,0x00,0x93,0x7B,0x15,0x45,0xB6,0x06,0x2E,0xAA,0x00,0x00,0x10,0xA4,
   0x40,0x6A,0x59,0x19,0xB8,0x98,0xBC,0x5D,0x8A,0x3E,0x80,0x5D,0x00,0x80,0x60,0x70,0x5A,0x6A,0xAC,0x3F,
   0xDB,0x60,0x3E,0xA4,0x03,0x66,0x81,0x4E,0x2D,0x01,0xB4,0x67,0x01,0x02,0x1A,0x00,0x00,0x0C,0x91,0xCA,
   0xD2,0x90,0x00};

static const uint8_t /* 'A' */ font_default_065[63] = \
  {0x00,0x00,0x04,0x5E,0x00,0x00,0x00,0x00,0x00,0x46,0xD5,0x01,0x00,0x00,0x00,0x00,0x9D,0xDC,0x34,0x00,
   0x00,0x00,0x06,0xD0,0x55,0x94,0x00,0x00,0x00,0x4B,0x8D,0x08,0xDC,0x07,0x00,0x00,0xA2,0x80,0x50,0xC4,
   0x53,0x00,0x08,0xE0,0x70,0x70,0x91,0xB2,0x00,0x50,0x9F,0x00,0x00,0x06,0xE6,0x17,0xA3,0x4C,0x00,0x00,
   0x00,0x8E,0x6F};

static const uint8_t /* 'B' */ font_default_066[63] = \
  {0x3E,0xB4,0xB4,0xA5,0x75,0x06,0x00,0x58,0xA9,0x21,0x34,0xA2,0x9C,0x00,0x58,0x9C,0x00,0x00,0x20,0xD9,
   0x00,0x58,0xA7,0x1D,0x31,0x9E,0x84,0x00,0x58,0xE3,0xB4,0xCA,0xE2,0x1F,0x00,0x58,0x9C,0x00,0x00,0x30,
   0xDC,0x02,0x58,0x9C,0x00,0x00,0x00,0xD3,0x2C,0x58,0x9C,0x00,0x00,0x3D,0xEC,0x07,0x57,0xF0,0xDC,0xE4,
   0xCF,0x35,0x00};

static const uint8_t /* 'C' */ font_default_067[63] = \
  {0x00,0x01,0x54,0xA3,0x7E,0x24,0x00,0x00,0x7D,0xAB,0x27,0x4F,0xE1,0x25,0x18,0xE0,0x08,0x00,0x00,0x2E,
   0x0A,0x48,0x9E,0x00,0x00,0x00,0x00,0x00,0x6A,0x84,0x00,0x00,0x00,0x00,0x00,0x54,0x99,0x00,0x00,0x00,
   0x00,0x00,0x2A,0xDA,0x03,0x00,0x00,0x00,0x00,0x00,0xAF,0x83,0x03,0x0F,0xA1,0x12,0x00,0x10,0x9E,0xDB,
   0xC7,0x57,0x00};

static const uint8_t /* 'D' */ font_default_068[63] = \
  {0x30,0xB4,0xB4,0xA3,0x4E,0x00,0x00,0x44,0xAE,0x26,0x46,0xCF,0x68,0x00,0x44,0xA0,0x00,0x00,0x20,0xE9,
   0x01,0x44,0xA0,0x00,0x00,0x00,0xD3,0x1F,0x44,0xA0,0x00,0x00,0x00,0xB4,0x46,0x44,0xA0,0x00,0x00,0x00,
   0xCB,0x3C,0x44,0xA0,0x00,0x00,0x08,0xEF,0x0E,0x44,0xA0,0x00,0x03,0x9D,0x94,0x00,0x43,0xE4,0xBE,0xCF,
   0x8F,0x08,0x00};

static const uint8_t /* 'E' */ font_default_069[63] = \
  {0x25,0xB4,0xB4,0xB4,0xB4,0xB4,0x06,0x34,0xC4,0x28,0x28,0x28,0x28,0x02,0x34,0xB8,0x00,0x00,0x00,0x00,
   0x00,0x34,0xB8,0x00,0x00,0x00,0x00,0x00,0x34,0xFA,0xE8,0xE8,0xE8,0x42,0x00,0x34,0xB8,0x00,0x00,0x00,
   0x00,0x00,0x34,0xB8,0x00,0x00,0x00,0x00,0x00,0x34,0xB8,0x00,0x00,0x00,0x00,0x00,0x34,0xF3,0xDC,0xDC,
   0xDC,0xDC,0x04};

static const uint8_t /* 'F' */ font_default_070[45] = \
  {0xA9,0xB4,0xB4,0xB4,0xB2,0xF0,0x2B,0x24,0x24,0x24,0xF0,0x08,0x00,0x00,0x00,0xF0,0x27,0x20,0x20,0x08,
   0xF0,0xBB,0xB8,0xB8,0x2C,0xF0,0x08,0x00,0x00,0x00,0xF0,0x08,0x00,0x00,0x00,0xF0,0x08,0x00,0x00,0x00,
   0xED,0x08,0x00,0x00,0x00};

static const uint8_t /* 'G' */ font_default_071[63] = \
  {0x00,0x01,0x59,0xA5,0x85,0x24,0x00,0x00,0x87,0xB0,0x3C,0x5D,0xDC,0x15,0x1F,0xD1,0x06,0x00,0x00,0x25,
   0x01,0x50,0x89,0x00,0x00,0x00,0x00,0x00,0x75,0x76,0x00,0x07,0x34,0x34,0x10,0x63,0x87,0x00,0x14,0x9C,
   0xD6,0x4C,0x38,0xD0,0x01,0x00,0x00,0x94,0x4C,0x01,0xBF,0x73,0x01,0x06,0xB0,0x4C,0x00,0x15,0xA5,0xD9,
   0xCF,0x86,0x0E};

static const uint8_t /* 'H' */ font_default_072[63] = \
  {0x30,0x89,0x00,0x00,0x00,0xA4,0x0C,0x44,0xBC,0x00,0x00,0x00,0xE8,0x08,0x44,0xBC,0x00,0x00,0x00,0xE8,
   0x08,0x44,0xBC,0x00,0x00,0x00,0xE8,0x08,0x44,0xF6,0xD8,0xD8,0xD8,0xFD,0x08,0x44,0xBC,0x00,0x00,0x00,
   0xEC,0x08,0x44,0xBC,0x00,0x00,0x00,0xEC,0x08,0x44,0xBC,0x00,0x00,0x00,0xEC,0x08,0x43,0xBA,0x00,0x00,
   0x00,0xE9,0x08};

static const uint8_t /* 'I' */ font_default_073[45] = \
  {0x90,0xB4,0xB4,0xB4,0x4F,0x1A,0x51,0xC8,0x20,0x0E,0x00,0x38,0xC0,0x00,0x00,0x00,0x38,0xC0,0x00,0x00,
   0x00,0x38,0xC0,0x00,0x00,0x00,0x38,0xC0,0x00,0x00,0x00,0x38,0xC0,0x00,0x00,0x00,0x38,0xC0,0x00,0x00,
   0xBA,0xDA,0xF0,0xCC,0x6D};

static const uint8_t /* 'J' */ font_default_074[63] = \
  {0x00,0x00,0x77,0xB4,0xB4,0xB4,0x2D,0x00,0x00,0x13,0x1C,0xF2,0x1C,0x07,0x00,0x00,0x00,0x00,0xF0,0x00,
   0x00,0x00,0x00,0x00,0x00,0xF0,0x00,0x00,0x00,0x00,0x00,0x00,0xF0,0x00,0x00,0x00,0x00,0x00,0x00,0xF0,
   0x00,0x00,0x00,0x00,0x00,0x08,0xEA,0x00,0x00,0x0E,0x60,0x03,0x58,0xB5,0x00,0x00,0x0F,0xA3,0xDC,0xBF,
   0x1C,0x00,0x00};

static const uint8_t /* 'K' */ font_default_075[70] = \
  {0x4C,0x70,0x00,0x00,0x25,0xA4,0x09,0x6C,0x98,0x00,0x15,0xD4,0x3C,0x00,0x6C,0x98,0x0A,0xC7,0x59,0x00,
   0x00,0x6C,0x9B,0xB0,0x7B,0x00,0x00,0x00,0x6C,0xF5,0xE7,0x50,0x00,0x00,0x00,0x6C,0xAA,0x2F,0xE5,0x18,
   0x00,0x00,0x6C,0x98,0x00,0x7C,0xB7,0x01,0x00,0x6C,0x98,0x00,0x05,0xCC,0x6C,0x00,0x6B,0x96,0x00,0x00,
   0x2B,0xF0,0x29,0x00,0x00,0x00,0x00,0x00,0x06,0x01};

static const uint8_t /* 'L' */ font_default_076[63] = \
  {0x11,0xA9,0x01,0x00,0x00,0x00,0x00,0x18,0xE4,0x00,0x00,0x00,0x00,0x00,0x18,0xE4,0x00,0x00,0x00,0x00,
   0x00,0x18,0xE4,0x00,0x00,0x00,0x00,0x00,0x18,0xE4,0x00,0x00,0x00,0x00,0x00,0x18,0xE4,0x00,0x00,0x00,
   0x00,0x00,0x18,0xE4,0x00,0x00,0x00,0x00,0x00,0x18,0xE4,0x00,0x00,0x00,0x00,0x00,0x18,0xF8,0xD4,0xD4,
   0xD4,0xD4,0x04};

static const uint8_t /* 'M' */ font_default_077[63] = \
  {0x52,0x56,0x00,0x00,0x00,0x6C,0x36,0x74,0xE1,0x06,0x00,0x14,0xF4,0x4C,0x74,0xEF,0x68,0x00,0x89,0xEA,
   0x4C,0x74,0x8A,0xD7,0x1C,0xC5,0x9E,0x4C,0x74,0x74,0x89,0xDA,0x4C,0x9C,0x4C,0x74,0x74,0x14,0xB2,0x01,
   0x9C,0x4C,0x74,0x74,0x00,0x00,0x00,0x9C,0x4C,0x74,0x74,0x00,0x00,0x00,0x9C,0x4C,0x73,0x73,0x00,0x00,
   0x00,0x9A,0x4B};

static const uint8_t /* 'N' */ font_default_078[63] = \
  {0x39,0x94,0x01,0x00,0x00,0x90,0x1E,0x50,0xFF,0x4B,0x00,0x00,0xCC,0x24,0x50,0xD0,0xD5,0x04,0x00,0xCC,
   0x24,0x50,0x9C,0xA8,0x67,0x00,0xCC,0x24,0x50,0x9C,0x22,0xE2,0x0D,0xCC,0x24,0x50,0x9C,0x00,0x8F,0x84,
   0xCC,0x24,0x50,0x9C,0x00,0x13,0xE6,0xE4,0x24,0x50,0x9C,0x00,0x00,0x75,0xFF,0x24,0x4F,0x9A,0x00,0x00,
   0x08,0xDF,0x24};

static const uint8_t /* 'O' */ font_default_079[63] = \
  {0x00,0x06,0x75,0xA4,0x68,0x06,0x00,0x01,0xB4,0x8E,0x43,0xB0,0xA8,0x00,0x40,0xBF,0x00,0x00,0x0A,0xE0,
   0x21,0x74,0x77,0x00,0x00,0x00,0x9C,0x61,0x93,0x60,0x00,0x00,0x00,0x85,0x73,0x7C,0x76,0x00,0x00,0x00,
   0x8D,0x69,0x53,0xB3,0x00,0x00,0x00,0xC4,0x35,0x05,0xD7,0x5B,0x06,0x6C,0xD2,0x02,0x00,0x24,0xBC,0xE9,
   0xAF,0x21,0x00};

static const uint8_t /* 'P' */ font_default_080[63] = \
  {0x25,0xB4,0xB4,0xB2,0x91,0x18,0x00,0x34,0xD6,0x34,0x3D,0x85,0xD6,0x03,0x34,0xCA,0x00,0x00,0x00,0xD2,
   0x2C,0x34,0xC9,0x00,0x00,0x13,0xE9,0x11,0x34,0xF0,0xB4,0xBA,0xE4,0x68,0x00,0x34,0xD4,0x24,0x23,0x0C,
   0x00,0x00,0x34,0xCC,0x00,0x00,0x00,0x00,0x00,0x34,0xCC,0x00,0x00,0x00,0x00,0x00,0x34,0xC9,0x00,0x00,
   0x00,0x00,0x00};

static const uint8_t /* 'Q' */ font_default_081[84] = \
  {0x00,0x05,0x73,0xA4,0x66,0x05,0x00,0x00,0xAE,0x8E,0x40,0xAF,0xA1,0x00,0x3A,0xC1,0x00,0x00,0x0A,0xE0,
   0x1B,0x71,0x76,0x00,0x00,0x00,0x99,0x5D,0x92,0x5C,0x00,0x00,0x00,0x7F,0x73,0x84,0x6E,0x00,0x00,0x00,
   0x8B,0x6C,0x61,0x9D,0x00,0x00,0x00,0xB1,0x44,0x0F,0xE7,0x28,0x00,0x2C,0xE2,0x07,0x00,0x4B,0xE4,0xBB,
   0xDE,0x3B,0x00,0x00,0x00,0x0D,0xDE,0x14,0x00,0x00,0x00,0x00,0x00,0x7B,0xDA,0xC6,0x09,0x00,0x00,0x00,
   0x00,0x13,0x24,0x02};

static const uint8_t /* 'R' */ font_default_082[63] = \
  {0x2B,0xB4,0xB4,0xB2,0x8B,0x0F,0x00,0x3C,0xCA,0x34,0x40,0x99,0xC0,0x00,0x3C,0xBC,0x00,0x00,0x02,0xEF,
   0x13,0x3C,0xBC,0x00,0x00,0x20,0xEE,0x07,0x3C,0xEC,0xB4,0xBD,0xE7,0x5D,0x00,0x3C,0xC6,0x24,0x7C,0x9B,
   0x00,0x00,0x3C,0xBC,0x00,0x0A,0xE2,0x1F,0x00,0x3C,0xBC,0x00,0x00,0x73,0x9A,0x00,0x3C,0xBA,0x00,0x00,
   0x0B,0xE3,0x21};

static const uint8_t /* 'S' */ font_default_083[63] = \
  {0x00,0x0B,0x76,0xA8,0x83,0x16,0x00,0x00,0xAF,0x89,0x2B,0x62,0xAB,0x00,0x01,0xF5,0x10,0x00,0x00,0x05,
   0x00,0x00,0xA0,0xB7,0x21,0x00,0x00,0x00,0x00,0x07,0x70,0xE4,0xA3,0x1B,0x00,0x00,0x00,0x00,0x07,0x6F,
   0xDD,0x0A,0x00,0x02,0x00,0x00,0x00,0xC8,0x34,0x1B,0xB3,0x1C,0x00,0x35,0xE3,0x0E,0x02,0x70,0xD2,0xDD,
   0xC5,0x39,0x00};

static const uint8_t /* 'T' */ font_default_084[63] = \
  {0x69,0xB4,0xB4,0xB4,0xB4,0xB4,0x3E,0x1A,0x2C,0x4E,0xD9,0x2C,0x2C,0x10,0x00,0x00,0x28,0xD0,0x00,0x00,
   0x00,0x00,0x00,0x28,0xD0,0x00,0x00,0x00,0x00,0x00,0x28,0xD0,0x00,0x00,0x00,0x00,0x00,0x28,0xD0,0x00,
   0x00,0x00,0x00,0x00,0x28,0xD0,0x00,0x00,0x00,0x00,0x00,0x28,0xD0,0x00,0x00,0x00,0x00,0x00,0x28,0xCD,
   0x00,0x00,0x00};

static const uint8_t /* 'U' */ font_default_085[63] = \
  {0x39,0x7A,0x00,0x00,0x00,0x7C,0x28,0x50,0xA0,0x00,0x00,0x00,0xB0,0x38,0x50,0xA0,0x00,0x00,0x00,0xB0,
   0x38,0x50,0xA0,0x00,0x00,0x00,0xB0,0x38,0x50,0xA0,0x00,0x00,0x00,0xB0,0x38,0x50,0xA1,0x00,0x00,0x00,
   0xB1,0x38,0x47,0xBB,0x00,0x00,0x00,0xCD,0x2D,0x10,0xE6,0x39,0x00,0x45,0xDD,0x03,0x00,0x33,0xC3,0xD7,
   0xBB,0x30,0x00};

static const uint8_t /* 'V' */ font_default_086[63] = \
  {0x62,0x53,0x00,0x00,0x00,0x5B,0x51,0x42,0xBE,0x00,0x00,0x00,0xC5,0x2A,0x04,0xE6,0x17,0x00,0x17,0xD2,
   0x00,0x00,0x96,0x69,0x00,0x64,0x80,0x00,0x00,0x41,0xBE,0x00,0xB4,0x2C,0x00,0x00,0x03,0xE5,0x22,0xCF,
   0x01,0x00,0x00,0x00,0x95,0xBA,0x82,0x00,0x00,0x00,0x00,0x40,0xFF,0x2E,0x00,0x00,0x00,0x00,0x03,0xBF,
   0x01,0x00,0x00};

static const uint8_t /* 'W' */ font_default_087[63] = \
  {0x80,0x1E,0x00,0x05,0x00,0x1F,0x72,0x94,0x4D,0x00,0x95,0x00,0x4A,0x80,0x6B,0x76,0x08,0xF7,0x1E,0x6C,
   0x58,0x43,0xA0,0x42,0xDE,0x62,0x8E,0x31,0x1A,0xC9,0x86,0x5E,0xA7,0xAF,0x0B,0x01,0xE3,0xB0,0x04,0xD9,
   0xB5,0x00,0x00,0xC9,0xA2,0x00,0xB0,0xB9,0x00,0x00,0xA1,0x60,0x00,0x6E,0x92,0x00,0x00,0x77,0x1D,0x00,
   0x2C,0x6A,0x00};

static const uint8_t /* 'X' */ font_default_088[63] = \
  {0x25,0x93,0x01,0x00,0x09,0xA0,0x06,0x01,0xC2,0x49,0x00,0x74,0x88,0x00,0x00,0x3A,0xD3,0x12,0xE0,0x14,
   0x00,0x00,0x00,0xB1,0xD0,0x85,0x00,0x00,0x00,0x00,0x43,0xFF,0x22,0x00,0x00,0x00,0x00,0x9F,0xE4,0x86,
   0x00,0x00,0x00,0x29,0xE2,0x20,0xE9,0x1B,0x00,0x00,0xAD,0x66,0x00,0x7B,0x9A,0x00,0x33,0xD7,0x05,0x00,
   0x0A,0xDD,0x27};

static const uint8_t /* 'Y' */ font_default_089[63] = \
  {0x4F,0x74,0x00,0x00,0x00,0x7F,0x38,0x12,0xE9,0x1A,0x00,0x1C,0xE0,0x06,0x00,0x84,0x90,0x00,0x8A,0x76,
   0x00,0x00,0x13,0xE9,0x25,0xE2,0x0F,0x00,0x00,0x00,0x84,0xE0,0x8C,0x00,0x00,0x00,0x00,0x13,0xFF,0x23,
   0x00,0x00,0x00,0x00,0x00,0xFC,0x10,0x00,0x00,0x00,0x00,0x00,0xFC,0x10,0x00,0x00,0x00,0x00,0x00,0xF9,
   0x10,0x00,0x00};

static const uint8_t /* 'Z' */ font_default_090[63] = \
  {0x1D,0xB4,0xB4,0xB4,0xB4,0xB4,0x18,0x07,0x2C,0x2C,0x2C,0x62,0xD1,0x04,0x00,0x00,0x00,0x03,0xCB,0x3D,
   0x00,0x00,0x00,0x00,0x6A,0xA3,0x00,0x00,0x00,0x00,0x16,0xE1,0x19,0x00,0x00,0x00,0x00,0xA0,0x73,0x00,
   0x00,0x00,0x00,0x3C,0xD5,0x06,0x00,0x00,0x00,0x04,0xD1,0x44,0x00,0x00,0x00,0x00,0x48,0xFA,0xDC,0xDC,
   0xDC,0xDD,0x51};

static const uint8_t /* '[' */ font_default_091[60] = \
  {0x10,0x4C,0x4C,0x4C,0x36,0x34,0xD9,0x88,0x88,0x60,0x34,0xAC,0x00,0x00,0x00,0x34,0xAC,0x00,0x00,0x00,
   0x34,0xAC,0x00,0x00,0x00,0x34,0xAC,0x00,0x00,0x00,0x34,0xAC,0x00,0x00,0x00,0x34,0xAC,0x00,0x00,0x00,
   0x34,0xAC,0x00,0x00,0x00,0x34,0xAC,0x00,0x00,0x00,0x34,0xD5,0x7C,0x7C,0x5A,0x11,0x50,0x50,0x50,0x3A
   };

static const uint8_t /* '\' */ font_default_092[66] = \
  {0x00,0x1C,0x00,0x00,0x00,0x00,0x07,0xE4,0x16,0x00,0x00,0x00,0x00,0x85,0x80,0x00,0x00,0x00,0x00,0x18,
   0xE4,0x0A,0x00,0x00,0x00,0x00,0x9C,0x68,0x00,0x00,0x00,0x00,0x29,0xD9,0x03,0x00,0x00,0x00,0x00,0xB3,
   0x50,0x00,0x00,0x00,0x00,0x3F,0xC4,0x00,0x00,0x00,0x00,0x01,0xCA,0x38,0x00,0x00,0x00,0x00,0x56,0xAC,
   0x00,0x00,0x00,0x00,0x04,0x64};

static const uint8_t /* ']' */ font_default_093[60] = \
  {0x42,0x4C,0x4C,0x4C,0x05,0x75,0x88,0x88,0xEA,0x10,0x00,0x00,0x00,0xD0,0x10,0x00,0x00,0x00,0xD0,0x10,
   0x00,0x00,0x00,0xD0,0x10,0x00,0x00,0x00,0xD0,0x10,0x00,0x00,0x00,0xD0,0x10,0x00,0x00,0x00,0xD0,0x10,
   0x00,0x00,0x00,0xD0,0x10,0x00,0x00,0x00,0xD0,0x10,0x6D,0x7C,0x7C,0xE8,0x10,0x46,0x50,0x50,0x50,0x05
   };

static const uint8_t /* '^' */ font_default_094[25] = \
  {0x00,0x01,0x7B,0x01,0x00,0x00,0x57,0xF9,0x49,0x00,0x0A,0xD1,0x42,0xCB,0x02,0x7B,0x6A,0x00,0x9F,0x51,
   0x10,0x03,0x00,0x0F,0x05};

static const uint8_t /* '_' */ font_default_095[14] = \
  {0x64,0xBC,0xBC,0xBC,0xBC,0xBC,0x4A,0x14,0x24,0x24,0x24,0x24,0x24,0x0F};

static const uint8_t /* '`' */ font_default_096[20] = \
  {0x02,0x2B,0x00,0x00,0x03,0xD4,0x0E,0x00,0x00,0x7D,0xA6,0x01,0x00,0x13,0xCE,0x19,0x00,0x00,0x00,0x00
   };

static const uint8_t /* 'a' */ font_default_097[42] = \
  {0x00,0x01,0x36,0x58,0x2B,0x00,0x00,0x7B,0xA6,0x74,0xBE,0x72,0x00,0x03,0x00,0x00,0x18,0xE6,0x00,0x17,
   0x7F,0xA8,0xB2,0xF3,0x10,0xD9,0x51,0x20,0x15,0xED,0x43,0xC9,0x00,0x00,0x55,0xF4,0x05,0x9F,0xB6,0xA0,
   0x89,0xEE};

static const uint8_t /* 'b' */ font_default_098[70] = \
  {0x0F,0x45,0x00,0x00,0x00,0x00,0x00,0x30,0xD1,0x00,0x00,0x00,0x00,0x00,0x30,0xD0,0x00,0x00,0x00,0x00,
   0x00,0x30,0xD0,0x0F,0x50,0x34,0x00,0x00,0x30,0xE2,0xA7,0x82,0xDB,0x79,0x00,0x30,0xF5,0x0C,0x00,0x21,
   0xF4,0x0F,0x30,0xD8,0x00,0x00,0x00,0xD9,0x36,0x30,0xD9,0x00,0x00,0x00,0xDB,0x26,0x30,0xFC,0x20,0x00,
   0x3D,0xD5,0x03,0x30,0xB2,0x9B,0xB7,0xC2,0x2C,0x00};

static const uint8_t /* 'c' */ font_default_099[49] = \
  {0x00,0x00,0x16,0x50,0x42,0x0A,0x00,0x00,0x4B,0xD8,0x84,0xA4,0xCF,0x10,0x07,0xE5,0x24,0x00,0x00,0x32,
   0x01,0x2D,0xD5,0x00,0x00,0x00,0x00,0x00,0x1B,0xED,0x05,0x00,0x00,0x00,0x00,0x01,0xBA,0x8D,0x05,0x0A,
   0x68,0x03,0x00,0x13,0xA1,0xE1,0xD6,0x78,0x03};

static const uint8_t /* 'd' */ font_default_100[70] = \
  {0x00,0x00,0x00,0x00,0x00,0x45,0x07,0x00,0x00,0x00,0x00,0x00,0xE8,0x05,0x00,0x00,0x00,0x00,0x00,0xE8,
   0x04,0x00,0x01,0x3A,0x50,0x10,0xE8,0x04,0x00,0x94,0xAD,0x7D,0xB6,0xF2,0x04,0x28,0xCE,0x02,0x00,0x32,
   0xFF,0x04,0x56,0x9B,0x00,0x00,0x08,0xFF,0x04,0x4B,0xAE,0x00,0x00,0x0F,0xFF,0x04,0x14,0xE5,0x20,0x00,
   0x6C,0xFF,0x04,0x00,0x49,0xCC,0xB5,0x72,0xF1,0x09};

static const uint8_t /* 'e' */ font_default_101[49] = \
  {0x00,0x00,0x29,0x5A,0x2E,0x00,0x00,0x00,0x76,0xBC,0x6B,0xC0,0x6A,0x00,0x1A,0xD9,0x05,0x00,0x0E,0xE1,
   0x02,0x49,0xEA,0xBC,0xBC,0xBC,0xEC,0x0B,0x3D,0xBE,0x00,0x00,0x00,0x00,0x00,0x0A,0xDC,0x43,0x00,0x0A,
   0x41,0x00,0x00,0x2D,0xBD,0xC9,0xC4,0x54,0x00};

static const uint8_t /* 'f' */ font_default_102[70] = \
  {0x00,0x00,0x00,0x0B,0x3C,0x20,0x00,0x00,0x00,0x2F,0xD4,0x8E,0xC6,0x60,0x00,0x00,0xAD,0x50,0x00,0x05,
   0x1F,0x00,0x00,0xC7,0x2D,0x00,0x00,0x00,0x20,0xC8,0xF4,0xD2,0xC8,0x2C,0x00,0x00,0x00,0xC8,0x2C,0x00,
   0x00,0x00,0x00,0x00,0xC8,0x2C,0x00,0x00,0x00,0x00,0x00,0xC8,0x2C,0x00,0x00,0x00,0x00,0x00,0xC8,0x2C,
   0x00,0x00,0x00,0x00,0x00,0xC5,0x2C,0x00,0x00,0x00};

static const uint8_t /* 'g' */ font_default_103[70] = \
  {0x00,0x17,0x8B,0x9E,0x41,0x7C,0x45,0x02,0xD2,0x61,0x4A,0xEB,0x65,0x15,0x1E,0xD2,0x00,0x00,0x9F,0x4C,
   0x00,0x03,0xDD,0x5C,0x44,0xDC,0x1B,0x00,0x00,0x81,0xAD,0xA4,0x36,0x00,0x00,0x00,0xDF,0x4B,0x36,0x29,
   0x03,0x00,0x03,0xB1,0x9C,0xAA,0xBB,0xCA,0x08,0x5E,0x86,0x00,0x00,0x00,0xC0,0x2F,0x33,0xE1,0x95,0x7E,
   0xB1,0xA9,0x03,0x00,0x0D,0x3F,0x4C,0x28,0x00,0x00};

static const uint8_t /* 'h' */ font_default_104[60] = \
  {0x04,0x4C,0x03,0x00,0x00,0x00,0x0C,0xF9,0x00,0x00,0x00,0x00,0x0C,0xF8,0x00,0x00,0x00,0x00,0x0C,0xF8,
   0x02,0x39,0x43,0x02,0x0C,0xFA,0x99,0x93,0xD0,0x81,0x0C,0xFF,0x38,0x00,0x22,0xD3,0x0C,0xFA,0x00,0x00,
   0x09,0xF0,0x0C,0xF8,0x00,0x00,0x08,0xF0,0x0C,0xF8,0x00,0x00,0x08,0xF0,0x0C,0xF5,0x00,0x00,0x08,0xED
   };

static const uint8_t /* 'i' */ font_default_105[50] = \
  {0x00,0x00,0x10,0x00,0x00,0x00,0x21,0xFF,0x19,0x00,0x00,0x01,0x2A,0x01,0x00,0x37,0x64,0x61,0x00,0x00,
   0x3E,0x75,0xF8,0x00,0x00,0x00,0x08,0xF8,0x00,0x00,0x00,0x08,0xF8,0x00,0x00,0x00,0x08,0xF8,0x00,0x00,
   0x00,0x08,0xF8,0x00,0x00,0x80,0xCE,0xFB,0xCC,0x5A};

static const uint8_t /* 'j' */ font_default_106[78] = \
  {0x00,0x00,0x00,0x00,0x0F,0x00,0x00,0x00,0x00,0x0D,0xFD,0x29,0x00,0x00,0x00,0x00,0x29,0x02,0x00,0x1D,
   0x64,0x64,0x64,0x05,0x00,0x21,0x74,0x74,0xF8,0x0C,0x00,0x00,0x00,0x00,0xF0,0x0C,0x00,0x00,0x00,0x00,
   0xF0,0x0C,0x00,0x00,0x00,0x00,0xF0,0x0C,0x00,0x00,0x00,0x00,0xF0,0x0C,0x00,0x00,0x00,0x00,0xF2,0x0B,
   0x00,0x0F,0x00,0x15,0xF2,0x02,0x21,0xDB,0x7E,0xC6,0x73,0x00,0x00,0x2D,0x66,0x36,0x00,0x00};

static const uint8_t /* 'k' */ font_default_107[77] = \
  {0x08,0x4A,0x01,0x00,0x00,0x00,0x00,0x18,0xE5,0x00,0x00,0x00,0x00,0x00,0x18,0xE4,0x00,0x00,0x00,0x00,
   0x00,0x18,0xE4,0x00,0x00,0x17,0x5E,0x03,0x18,0xE4,0x00,0x1E,0xD0,0x47,0x00,0x18,0xE4,0x25,0xD3,0x3E,
   0x00,0x00,0x18,0xF3,0xDC,0xC3,0x04,0x00,0x00,0x18,0xF3,0x1F,0xB4,0x8E,0x00,0x00,0x18,0xE4,0x00,0x11,
   0xDD,0x5D,0x00,0x18,0xE1,0x00,0x00,0x30,0xEE,0x34,0x00,0x00,0x00,0x00,0x00,0x06,0x01};

static const uint8_t /* 'l' */ font_default_108[50] = \
  {0x41,0x4C,0x47,0x00,0x00,0x70,0x8C,0xEC,0x00,0x00,0x00,0x10,0xEC,0x00,0x00,0x00,0x10,0xEC,0x00,0x00,
   0x00,0x10,0xEC,0x00,0x00,0x00,0x10,0xEC,0x00,0x00,0x00,0x10,0xEC,0x00,0x00,0x00,0x10,0xEC,0x00,0x00,
   0x00,0x10,0xEC,0x00,0x00,0xC0,0xCF,0xF9,0xCC,0xA0};

static const uint8_t /* 'm' */ font_default_109[49] = \
  {0x32,0x33,0x51,0x1D,0x2F,0x4E,0x02,0x80,0xCE,0x60,0xE6,0x7D,0xBD,0x56,0x80,0x79,0x02,0xF9,0x0A,0x80,
   0x73,0x80,0x68,0x00,0xEC,0x00,0x7C,0x74,0x80,0x68,0x00,0xEC,0x00,0x7C,0x74,0x80,0x68,0x00,0xEC,0x00,
   0x7C,0x74,0x7E,0x67,0x00,0xE9,0x00,0x7B,0x73};

static const uint8_t /* 'n' */ font_default_110[42] = \
  {0x07,0x5E,0x05,0x3F,0x42,0x02,0x10,0xF5,0x9F,0x73,0xBF,0x82,0x10,0xFF,0x30,0x00,0x24,0xD0,0x10,0xF4,
   0x01,0x00,0x11,0xE8,0x10,0xF0,0x00,0x00,0x10,0xE8,0x10,0xF0,0x00,0x00,0x10,0xE8,0x10,0xED,0x00,0x00,
   0x10,0xE5};

static const uint8_t /* 'o' */ font_default_111[49] = \
  {0x00,0x00,0x27,0x57,0x28,0x00,0x00,0x00,0x78,0xC3,0x7B,0xD1,0x6F,0x00,0x29,0xDD,0x07,0x00,0x14,0xEB,
   0x14,0x64,0xA4,0x00,0x00,0x00,0xBF,0x41,0x55,0xBA,0x00,0x00,0x00,0xCD,0x31,0x10,0xE5,0x46,0x00,0x4B,
   0xD6,0x05,0x00,0x2E,0xC1,0xD9,0xBC,0x26,0x00};

static const uint8_t /* 'p' */ font_default_112[70] = \
  {0x13,0x52,0x13,0x51,0x3C,0x01,0x00,0x30,0xE9,0x93,0x5A,0xBA,0x95,0x00,0x30,0xEF,0x04,0x00,0x0B,0xED,
   0x23,0x2F,0xD7,0x00,0x00,0x00,0xC0,0x4D,0x2F,0xE9,0x00,0x00,0x00,0xCF,0x35,0x2E,0xFF,0x45,0x00,0x51,
   0xDB,0x07,0x2E,0xD7,0x8D,0xDB,0xCB,0x30,0x00,0x2E,0xD4,0x00,0x00,0x00,0x00,0x00,0x2D,0xD4,0x00,0x00,
   0x00,0x00,0x00,0x0F,0x46,0x00,0x00,0x00,0x00,0x00};

static const uint8_t /* 'q' */ font_default_113[70] = \
  {0x00,0x00,0x37,0x57,0x18,0x58,0x05,0x00,0x97,0x8D,0x53,0xB6,0xF2,0x0C,0x32,0xC5,0x00,0x00,0x21,0xFF,
   0x0C,0x61,0x99,0x00,0x00,0x03,0xFE,0x0C,0x4D,0xB6,0x00,0x00,0x0D,0xFF,0x0C,0x0E,0xE8,0x40,0x04,0x86,
   0xFE,0x0C,0x00,0x3C,0xD3,0xD7,0x69,0xEC,0x0C,0x00,0x00,0x00,0x00,0x00,0xEC,0x0C,0x00,0x00,0x00,0x00,
   0x00,0xEC,0x0C,0x00,0x00,0x00,0x00,0x00,0x4E,0x04};

static const uint8_t /* 'r' */ font_default_114[42] = \
  {0x41,0x27,0x1F,0x56,0x37,0x00,0xA4,0xAC,0xB8,0x64,0xB9,0x1B,0xA4,0xC5,0x06,0x00,0x02,0x00,0xA4,0x65,
   0x00,0x00,0x00,0x00,0xA4,0x58,0x00,0x00,0x00,0x00,0xA4,0x58,0x00,0x00,0x00,0x00,0xA2,0x57,0x00,0x00,
   0x00,0x00};

static const uint8_t /* 's' */ font_default_115[49] = \
  {0x00,0x00,0x2E,0x5A,0x37,0x01,0x00,0x00,0x77,0xBB,0x65,0xA5,0xA0,0x00,0x00,0xBD,0x6E,0x01,0x00,0x1B,
   0x00,0x00,0x2F,0xC5,0xDA,0x81,0x15,0x00,0x00,0x00,0x00,0x25,0x91,0xCE,0x00,0x0B,0x90,0x13,0x00,0x2D,
   0xE5,0x01,0x06,0x73,0xD2,0xD6,0xC9,0x3A,0x00};

static const uint8_t /* 't' */ font_default_116[63] = \
  {0x00,0x00,0x0F,0x24,0x00,0x00,0x00,0x00,0x00,0x78,0x8A,0x00,0x00,0x00,0x05,0x64,0xB6,0xAB,0x64,0x2E,
   0x00,0x06,0x6C,0xC4,0xA6,0x6C,0x31,0x00,0x00,0x00,0x9F,0x5A,0x00,0x00,0x00,0x00,0x00,0xA9,0x4D,0x00,
   0x00,0x00,0x00,0x00,0xB2,0x4C,0x00,0x00,0x00,0x00,0x00,0x9E,0x90,0x03,0x44,0x00,0x00,0x00,0x2F,0xD9,
   0xE3,0x99,0x04};

static const uint8_t /* 'u' */ font_default_117[49] = \
  {0x10,0x56,0x00,0x00,0x04,0x60,0x00,0x28,0xDC,0x00,0x00,0x08,0xF4,0x00,0x28,0xDC,0x00,0x00,0x08,0xF4,
   0x00,0x28,0xDC,0x00,0x00,0x08,0xF4,0x00,0x25,0xEA,0x00,0x00,0x1C,0xF4,0x00,0x07,0xEF,0x25,0x00,0x71,
   0xF4,0x00,0x00,0x45,0xC0,0xA6,0x6E,0xF6,0x01};

static const uint8_t /* 'v' */ font_default_118[49] = \
  {0x28,0x46,0x00,0x00,0x00,0x47,0x13,0x22,0xE4,0x04,0x00,0x04,0xD9,0x06,0x00,0xBC,0x4D,0x00,0x40,0x9F,
   0x00,0x00,0x58,0xB2,0x00,0x9A,0x45,0x00,0x00,0x08,0xE9,0x2B,0xCE,0x02,0x00,0x00,0x00,0x91,0xDC,0x6D,
   0x00,0x00,0x00,0x00,0x2D,0xF0,0x0E,0x00,0x00};

static const uint8_t /* 'w' */ font_default_119[49] = \
  {0x4C,0x14,0x00,0x00,0x00,0x22,0x37,0xA0,0x43,0x01,0xA9,0x00,0x6B,0x6F,0x6D,0x73,0x26,0xFF,0x26,0x89,
   0x46,0x3A,0xA5,0x63,0xA6,0x72,0xA7,0x1C,0x0B,0xD3,0x9A,0x22,0xBB,0xB6,0x01,0x00,0xD2,0xA5,0x00,0xC7,
   0xB4,0x00,0x00,0x9F,0x7C,0x00,0x82,0x9E,0x00};

static const uint8_t /* 'x' */ font_default_120[49] = \
  {0x12,0x59,0x01,0x00,0x11,0x54,0x00,0x01,0xB4,0x60,0x00,0xA0,0x63,0x00,0x00,0x19,0xDE,0x6A,0xBB,0x01,
   0x00,0x00,0x00,0x5F,0xFF,0x24,0x00,0x00,0x00,0x01,0xAB,0xD7,0x7D,0x00,0x00,0x00,0x5A,0xBC,0x0F,0xDA,
   0x37,0x00,0x19,0xDE,0x20,0x00,0x3C,0xD5,0x0C};

static const uint8_t /* 'y' */ font_default_121[70] = \
  {0x1E,0x53,0x00,0x00,0x00,0x49,0x1D,0x10,0xEA,0x0E,0x00,0x03,0xE6,0x12,0x00,0x9D,0x68,0x00,0x3C,0xB2,
   0x00,0x00,0x35,0xCD,0x00,0x95,0x51,0x00,0x00,0x01,0xCE,0x36,0xD6,0x06,0x00,0x00,0x00,0x66,0xD5,0x91,
   0x00,0x00,0x00,0x00,0x0D,0xF4,0x31,0x00,0x00,0x02,0x00,0x25,0xCA,0x01,0x00,0x00,0x78,0x96,0xDB,0x47,
   0x00,0x00,0x00,0x0C,0x48,0x21,0x00,0x00,0x00,0x00};

static const uint8_t /* 'z' */ font_default_122[49] = \
  {0x05,0x64,0x64,0x64,0x64,0x55,0x00,0x06,0x7C,0x7C,0x7C,0xD2,0xA6,0x00,0x00,0x00,0x00,0x46,0xD4,0x0E,
   0x00,0x00,0x00,0x23,0xDF,0x28,0x00,0x00,0x00,0x0C,0xD1,0x50,0x00,0x00,0x00,0x01,0xAB,0x83,0x00,0x00,
   0x00,0x00,0x46,0xFC,0xDF,0xDC,0xDC,0xDE,0x26};

static const uint8_t /* '{' */ font_default_123[72] = \
  {0x00,0x00,0x12,0x9E,0xC8,0x79,0x00,0x00,0x90,0x74,0x00,0x00,0x00,0x00,0xC3,0x34,0x00,0x00,0x00,0x00,
   0xC1,0x2E,0x00,0x00,0x00,0x1B,0xD6,0x06,0x00,0x00,0x3A,0xEA,0x60,0x00,0x00,0x00,0x00,0x0C,0xDF,0x0A,
   0x00,0x00,0x00,0x00,0xCC,0x2A,0x00,0x00,0x00,0x00,0xCE,0x28,0x00,0x00,0x00,0x00,0xBA,0x4A,0x00,0x00,
   0x00,0x00,0x43,0xE0,0x98,0x54,0x00,0x00,0x00,0x10,0x35,0x21};

static const uint8_t /* '|' */ font_default_124[26] = \
  {0x01,0x1C,0x08,0xE0,0x08,0xE0,0x08,0xE0,0x08,0xE0,0x08,0xE0,0x08,0xE0,0x08,0xE0,0x08,0xE0,0x08,0xE0,
   0x08,0xE0,0x08,0xE0,0x01,0x1C};

static const uint8_t /* '}' */ font_default_125[72] = \
  {0x96,0xCA,0x8D,0x06,0x00,0x00,0x00,0x01,0x9A,0x6B,0x00,0x00,0x00,0x00,0x59,0x9B,0x00,0x00,0x00,0x00,
   0x53,0x9C,0x00,0x00,0x00,0x00,0x1E,0xCE,0x0F,0x00,0x00,0x00,0x00,0x86,0xE2,0x1D,0x00,0x00,0x27,0xCC,
   0x03,0x00,0x00,0x00,0x53,0xA4,0x00,0x00,0x00,0x00,0x50,0xA9,0x00,0x00,0x00,0x00,0x73,0x94,0x00,0x00,
   0x68,0x9E,0xE3,0x24,0x00,0x00,0x29,0x34,0x09,0x00,0x00,0x00};

static const uint8_t /* '~' */ font_default_126[21] = \
  {0x00,0x16,0x52,0x14,0x00,0x0C,0x03,0x27,0xD3,0x90,0xDD,0x72,0xC3,0x40,0x06,0x1C,0x00,0x27,0x82,0x3A,
   0x00};


// number of entries in a font definition
#define FONT_ENTRIES 95 

// Define font information structure if it hasn't been done already (user might include a font header before xwin.h)
#ifndef XWIN_FONT_INFO
#define XWIN_FONT_INFO
typedef struct {
    uint16_t  width, height; // width/height of character alpha mask
    int16_t   top,   left;   // how far to offset pen for this character
    int16_t   advx,  advy;   // how far to actually advance pen for this character
    const uint8_t *mask;          // character alpha mask
} font_info_t;
#endif

// Array of font_default_info_t describes an ASCII font
static const font_info_t font_default[95] = {
  {  0,   0,   0,   0,   7,   0, NULL},
  {  3,  10,  10,   2,   7,   0, (uint8_t*)&font_default_033}, // '!'
  {  5,   5,  10,   1,   7,   0, (uint8_t*)&font_default_034}, // '"'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_035}, // '#'
  {  7,  11,  10,   0,   7,   0, (uint8_t*)&font_default_036}, // '$'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_037}, // '%'
  {  7,  10,   9,   0,   7,   0, (uint8_t*)&font_default_038}, // '&'
  {  3,   5,  10,   2,   7,   0, (uint8_t*)&font_default_039}, // '''
  {  5,  13,  10,   1,   7,   0, (uint8_t*)&font_default_040}, // '('
  {  5,  13,  10,   0,   7,   0, (uint8_t*)&font_default_041}, // ')'
  {  7,   7,   8,   0,   7,   0, (uint8_t*)&font_default_042}, // '*'
  {  7,   7,   8,   0,   7,   0, (uint8_t*)&font_default_043}, // '+'
  {  3,   5,   2,   2,   7,   0, (uint8_t*)&font_default_044}, // ','
  {  6,   2,   5,   0,   7,   0, (uint8_t*)&font_default_045}, // '-'
  {  3,   2,   2,   2,   7,   0, (uint8_t*)&font_default_046}, // '.'
  {  6,  11,  10,   0,   7,   0, (uint8_t*)&font_default_047}, // '/'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_048}, // '0'
  {  4,   9,   9,   1,   7,   0, (uint8_t*)&font_default_049}, // '1'
  {  6,   9,   9,   1,   7,   0, (uint8_t*)&font_default_050}, // '2'
  {  6,   9,   9,   0,   7,   0, (uint8_t*)&font_default_051}, // '3'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_052}, // '4'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_053}, // '5'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_054}, // '6'
  {  5,   9,   9,   1,   7,   0, (uint8_t*)&font_default_055}, // '7'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_056}, // '8'
  {  6,   9,   9,   0,   7,   0, (uint8_t*)&font_default_057}, // '9'
  {  3,   7,   7,   2,   7,   0, (uint8_t*)&font_default_058}, // ':'
  {  3,   9,   6,   2,   7,   0, (uint8_t*)&font_default_059}, // ';'
  {  7,   8,   8,   0,   7,   0, (uint8_t*)&font_default_060}, // '<'
  {  7,   5,   7,   0,   7,   0, (uint8_t*)&font_default_061}, // '='
  {  7,   8,   8,   0,   7,   0, (uint8_t*)&font_default_062}, // '>'
  {  6,  10,  10,   0,   7,   0, (uint8_t*)&font_default_063}, // '?'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_064}, // '@'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_065}, // 'A'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_066}, // 'B'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_067}, // 'C'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_068}, // 'D'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_069}, // 'E'
  {  5,   9,   9,   1,   7,   0, (uint8_t*)&font_default_070}, // 'F'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_071}, // 'G'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_072}, // 'H'
  {  5,   9,   9,   1,   7,   0, (uint8_t*)&font_default_073}, // 'I'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_074}, // 'J'
  {  7,  10,   9,   0,   7,   0, (uint8_t*)&font_default_075}, // 'K'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_076}, // 'L'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_077}, // 'M'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_078}, // 'N'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_079}, // 'O'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_080}, // 'P'
  {  7,  12,   9,   0,   7,   0, (uint8_t*)&font_default_081}, // 'Q'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_082}, // 'R'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_083}, // 'S'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_084}, // 'T'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_085}, // 'U'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_086}, // 'V'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_087}, // 'W'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_088}, // 'X'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_089}, // 'Y'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_090}, // 'Z'
  {  5,  12,  10,   1,   7,   0, (uint8_t*)&font_default_091}, // '['
  {  6,  11,  10,   0,   7,   0, (uint8_t*)&font_default_092}, // '\'
  {  5,  12,  10,   1,   7,   0, (uint8_t*)&font_default_093}, // ']'
  {  5,   5,   9,   1,   7,   0, (uint8_t*)&font_default_094}, // '^'
  {  7,   2,   0,   0,   7,   0, (uint8_t*)&font_default_095}, // '_'
  {  4,   5,  10,   1,   7,   0, (uint8_t*)&font_default_096}, // '`'
  {  6,   7,   7,   0,   7,   0, (uint8_t*)&font_default_097}, // 'a'
  {  7,  10,  10,   0,   7,   0, (uint8_t*)&font_default_098}, // 'b'
  {  7,   7,   7,   0,   7,   0, (uint8_t*)&font_default_099}, // 'c'
  {  7,  10,  10,   0,   7,   0, (uint8_t*)&font_default_100}, // 'd'
  {  7,   7,   7,   0,   7,   0, (uint8_t*)&font_default_101}, // 'e'
  {  7,  10,  10,   0,   7,   0, (uint8_t*)&font_default_102}, // 'f'
  {  7,  10,   7,   0,   7,   0, (uint8_t*)&font_default_103}, // 'g'
  {  6,  10,  10,   0,   7,   0, (uint8_t*)&font_default_104}, // 'h'
  {  5,  10,  10,   1,   7,   0, (uint8_t*)&font_default_105}, // 'i'
  {  6,  13,  10,   0,   7,   0, (uint8_t*)&font_default_106}, // 'j'
  {  7,  11,  10,   0,   7,   0, (uint8_t*)&font_default_107}, // 'k'
  {  5,  10,  10,   1,   7,   0, (uint8_t*)&font_default_108}, // 'l'
  {  7,   7,   7,   0,   7,   0, (uint8_t*)&font_default_109}, // 'm'
  {  6,   7,   7,   0,   7,   0, (uint8_t*)&font_default_110}, // 'n'
  {  7,   7,   7,   0,   7,   0, (uint8_t*)&font_default_111}, // 'o'
  {  7,  10,   7,   0,   7,   0, (uint8_t*)&font_default_112}, // 'p'
  {  7,  10,   7,   0,   7,   0, (uint8_t*)&font_default_113}, // 'q'
  {  6,   7,   7,   1,   7,   0, (uint8_t*)&font_default_114}, // 'r'
  {  7,   7,   7,   0,   7,   0, (uint8_t*)&font_default_115}, // 's'
  {  7,   9,   9,   0,   7,   0, (uint8_t*)&font_default_116}, // 't'
  {  7,   7,   7,   0,   7,   0, (uint8_t*)&font_default_117}, // 'u'
  {  7,   7,   7,   0,   7,   0, (uint8_t*)&font_default_118}, // 'v'
  {  7,   7,   7,   0,   7,   0, (uint8_t*)&font_default_119}, // 'w'
  {  7,   7,   7,   0,   7,   0, (uint8_t*)&font_default_120}, // 'x'
  {  7,  10,   7,   0,   7,   0, (uint8_t*)&font_default_121}, // 'y'
  {  7,   7,   7,   0,   7,   0, (uint8_t*)&font_default_122}, // 'z'
  {  6,  12,   9,   0,   7,   0, (uint8_t*)&font_default_123}, // '{'
  {  2,  13,  10,   2,   7,   0, (uint8_t*)&font_default_124}, // '|'
  {  6,  12,   9,   1,   7,   0, (uint8_t*)&font_default_125}, // '}'
  {  7,   3,   7,   0,   7,   0, (uint8_t*)&font_default_126}, // '~'
 };


/*******************************************************************************
 * Returns the width and height of the bounding box the encloses the given text
 * for the given font with specified text length.
 *
 * @param font  font to use when rendering text
 * @param text  text to render
 * @param len   length of text
 * @param ww    pointer to variable to hold width of text bounding box
 * @param hh    pointer to variable to hold height of text bounding box
 * @param top   optional pointer to variable to hold maximum y value of top of text (used for rendering)
 *******************************************************************************/
static inline void font_bboxn(const font_info_t *font, const char* text, size_t len, uint16_t *ww, uint16_t *hh, uint16_t *top) {
    *ww=0;
    *hh=0;

    // Don't draw more than length of string
    size_t rlen = strlen(text);
    if (len > rlen) {
        len = rlen;
    }

    // TODO: Need to make sure that characters in the string are in-range for font
    int16_t maxy=0, miny=0x7FFF;
    for (size_t ii=0; ii < len; ii++) {
        font_info_t font_info = font[text[ii]-32];

        *ww += font_info.advx;

        // Fonts may extend below zero line in the case of letters like 'y', 'g', and 'q'.
        // So we have to take this into account when figuring out the height of the bounding box.
        int16_t bottom = font_info.top - font_info.height;
        maxy = (font_info.top > maxy) ? font_info.top : maxy;
        miny = (bottom        < miny) ? bottom        : miny;
    }

    *hh = maxy-miny;

    // Return maximum y value in the string
    if (top != NULL) {
        *top = maxy;
    }
}


/*******************************************************************************
 * Returns the width and height of the bounding box the encloses the given text
 * for the given font with specified text length.
 *
 * @param font  font to use when rendering text
 * @param text  text to render
 * @param ww    pointer to variable to hold width of text bounding box
 * @param hh    pointer to variable to hold height of text bounding box
 *******************************************************************************/
static inline void font_bbox(const font_info_t *font, const char* text, uint16_t *ww, uint16_t *hh) {
    font_bboxn(font, text, strlen(text), ww, hh, NULL);
}


/*******************************************************************************
 * Return maximum height of any character above the baseline for the given font
 *
 * @param font font to query
 *
 * @return maximum height off baseline that can be expected
 *******************************************************************************/
static inline int font_maxy(const font_info_t *font) {
    int max=0;
    for (size_t ii=0; ii < FONT_ENTRIES; ii++) {
        int yy = font[ii].top;
        max = (yy > max) ? yy : max;
    }
    return max;
}


/*******************************************************************************
 * Return maximum height of any character below the baseline for the given font
 *
 * @param font font to query
 *
 * @return minimum height below baseline that can be expected
 *******************************************************************************/
static inline int font_miny(const font_info_t *font) {
    int min=0xFFFFFFFF;
    for (size_t ii=0; ii < FONT_ENTRIES; ii++) {
        int yy = font[ii].top-font[ii].height;
        min = (yy < min) ? yy : min;
    }
    return min;
}


/*******************************************************************************
 * Returns the width and height of the bounding box the encloses the given text
 * for the default font with specified text length.
 *
 * @param text  text to render
 * @param len   length of text
 * @param ww    pointer to variable to hold width of text bounding box
 * @param hh    pointer to variable to hold height of text bounding box
 *******************************************************************************/
static inline void text_bboxn(const char* text, size_t len, uint16_t *ww, uint16_t *hh) {
    font_bboxn(font_default, text, len, ww, hh, NULL);
}


/*******************************************************************************
 * Returns the width and height of the bounding box the encloses the given text
 * for the default font with specified text length.
 *
 * @param text  text to render
 * @param ww    pointer to variable to hold width of text bounding box
 * @param hh    pointer to variable to hold height of text bounding box
 *******************************************************************************/
static inline void text_bbox(const char* text, uint16_t *ww, uint16_t *hh) {
    font_bboxn(font_default, text, strlen(text), ww, hh, NULL);
}


/*******************************************************************************
 * Draws text using a given font to a pixbuf using a given length
 *  
 * @param font    pointer font_info_t structure to use
 * @param pb      pointer to pixbuf_t instance to draw into
 * @param color   color of text
 * @parma x0      x coordinate of top,left corner of string
 * @param y0      y coordinate of top,left corner of string 
 * @param string  text to render
 * @param len     length of string to draw
 *******************************************************************************/
static inline void draw_fontn(const font_info_t *font, pixbuf_t *pb, pixel_t     color,
                                    int16_t      x0,   int16_t   y0, const char* string, size_t len) {
    uint16_t  xc=x0, yc=y0;

    // Don't draw more than length of string
    size_t rlen = strlen(string);
    if (len > rlen) {
        len = rlen;
    }

    pixel_t raw_color = pixel_unset_alpha(color);
    
    // TODO: Need to make sure that characters in the string are in-range for font
    uint16_t hh,ww,top; font_bboxn(font, string, len, &ww, &hh, &top);

    uint8_t alpha = color.a;
    for (size_t ii=0; ii < len; ii++) {
        font_info_t font_info = font[string[ii]-32];

        const uint8_t *mptr = font_info.mask;
        //uint16_t yl         = yc+top-font_info.top,  yh=yl+font_info.height;
        uint16_t yl=yc-font_info.top,  yh=yl+font_info.height;
        
        for (uint16_t yy=yl; yy < yh; yy++, mptr += font_info.width) {
            // If this line is clipped, move to the next one
            if (pixbuf_clip_y(pb, yy) != yy) {
                continue;
            }
            
            //uint16_t xl=xc+font_info.left,  xh=xl+font_info.width;
            uint16_t xl=xc+font_info.left,  xh=xl+font_info.width;
            xl = pixbuf_clip_x(pb, xl);
            xh = pixbuf_clip_x(pb, xh);

            for (uint16_t xx=xl; xx < xh; xx++) {
                color.a = MUL8(alpha, mptr[xx-(xc+font_info.left)]);
                draw_pixel(pb, pixel_set_alpha(raw_color, color.a), xx, yy);
            }
        }
        xc += font_info.advx;
    }
}


/*******************************************************************************
 * Draws text using a given font to a pixbuf
 *  
 * @param font    pointer font_info_t structure to use
 * @param pb      pointer to pixbuf_t instance to draw into
 * @param color   color of text
 * @parma x0      x coordinate of top,left corner of string
 * @param y0      y coordinate of top,left corner of string 
 * @param string  text to render
 *******************************************************************************/
static inline void draw_font(const font_info_t *font, pixbuf_t *pb, pixel_t     color,
                                   int16_t      x0,   int16_t   y0, const char* string) {
    draw_fontn(font, pb, color, x0, y0, string, strlen(string));
}


/*******************************************************************************
 * Draws text using the default font to a pixbuf using a given length
 *  
 * @param font    pointer font_info_t structure to use
 * @param pb      pointer to pixbuf_t instance to draw into
 * @param color   color of text
 * @parma x0      x coordinate of top,left corner of string
 * @param y0      y coordinate of top,left corner of string 
 * @param string  text to render
 * @param len     length of string to draw
 *******************************************************************************/
static inline void draw_textn(pixbuf_t *pb, pixel_t color,
                              int16_t   x0, int16_t y0, const char* string, size_t len) {
    draw_fontn(font_default, pb, color, x0, y0, string, len);
}


/*******************************************************************************
 * Draws text using the default font to a pixbuf 
 *  
 * @param font    pointer font_info_t structure to use
 * @param pb      pointer to pixbuf_t instance to draw into
 * @param color   color of text
 * @parma x0      x coordinate of top,left corner of string
 * @param y0      y coordinate of top,left corner of string 
 * @param string  text to render 
 *******************************************************************************/
static inline void draw_text(pixbuf_t *pb, pixel_t color,
                             int16_t   x0, int16_t y0, const char* string) {
    draw_fontn(font_default, pb, color, x0, y0, string, strlen(string));
}


#endif// XWIN_
